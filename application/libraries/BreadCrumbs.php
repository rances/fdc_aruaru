<?php

class BreadCrumbs{

    public $CI;
    public $cdb;
    public $bread_name;
    public $column = '';
    public $array_url = array();
    public $isParent = true;

    function __construct() {
        $this->CI =& get_instance();
        $this->cdb = $this->CI->db;
        $this->setColumn('
            big_cat.id as big_id,
            big_cat.name as m_cat,
            big_cat.jp_name as m_jpname
        ');
    }

    public function staticUrL($url = array()) {
       $this->array_url[] = array(
            'big_id' => $url['big_id'],
            'm_jpname' => $url['m_jpname']
        );
        return $this->set_records($this->array_url);
    }

    public function setColumn($data = '') {
        $this->column .= $data;
    }

    public function getColumn() {
        return $this->column;
    }

    public function getCategory() {
        $this->setJoinSubCategory();
        return $this;
    }

    public function setJoinSubCategory() {
         $this->setColumn('
            ,sub_cat.id as s_id,
            sub_cat.name as s_cat,
            sub_cat.jp_name as s_jpname,'
            );
         $this->cdb->join(
            'aruaru_bbs_categorys as sub_cat',
            'sub_cat.big_category_id = big_cat.id', 
            'LEFT'
            );
        return $this;
    }

    public function setJoinThread() {
        $this->setColumn('bb_thread.id,bb_thread.title');
        $this->cdb->join(
            'aruaru_bbs_threads as bb_thread',
            'bb_thread.cate_id = sub_cat.id', 
            'LEFT'
            );
        return $this;
    }

    public function setWhere($column = null, $value = "") {
        $this->cdb->where($column, $value);
        return $this;
    }

    public function get() {
        try {
            $this->cdb->select($this->getColumn());
            $this->cdb->from('aruaru_bbs_big_categorys as big_cat ');
            $this->cdb->group_by('big_cat.id');
            $query = $this->cdb->get();
            return $this->set_records($query->result_array());
        } catch (Exception $e) {
            return;
        }
    }

    public function display($data = null) {
        if (!empty($data)) {
            $this->CI->viewData['isBreadCrumb'] = true;
            $this->CI->viewData[$this->bread_name] = $data;
        }
    }

    public function set_records($data = array()) {
        $url_list[] = array('url' => '/', 'text' => 'トップページ');
        foreach ($data as $value) {
            $url_list[] = array('url' => base_url().'bulletin/'.$value['big_id'], 'text' => $value['m_jpname']);
            if ($this->isParent) {
                if (isset($value['s_jpname'])) {
                    $url_list[] = array('url' => base_url().'category/'.$value['s_id'], 'text' => $value['s_jpname']);
                }
                if (isset($value['title'])) {
                    $url_list[] = array('url' => base_url().'/'.$value['s_id'], 'text' => $value['title']);
                }
            }
        }
        $this->display($url_list);
    }
}