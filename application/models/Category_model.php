<?php
    class Category_model extends CI_Model
    {
        function __construct() {
            parent::__construct();
        }

        /**
         * Get main category for bulletin
         * @param string $bid: id of main category
         * @param string $columns: default value "*" this select the field (ex: "name, title")
         * @return multitype:
         */
        function getRecordsByBulletin($bid = null, $columns = "*") { 
            if(empty($bid)) return array();
            $sql = "
                SELECT * 
                FROM aruaru_bbs_categorys
                WHERE big_category_id = ?
                AND board_id IS NOT NULL
                AND jp_name <> ''
                ORDER BY `priority` ASC, `id`
            ";

            $query = $this->db->query($sql, $bid);
            return $query->result_array();  
      
        }

        /**
         * Get category by id
         * @param string $id
         * @return multitype:
         */
        function getRowById($id = null) {
            $sql = '
                SELECT * 
                FROM `aruaru_bbs_categorys`
                WHERE `id` = ?
                LIMIT 1
            ';
            $query = $this->db->query($sql, $id);
            return $query->row_array();   
        }        
        
        /**
         * Retrieve all the detail in category main and its 
         * sub category (aruaru_bbs_big_categorys) 
         * @param unknown $cat_id : id of main category
         * @return multitype: array query result
         */
        function getCateDetail($cat_id) {
            if (empty($cat_id)) return array();

            $sql = "
                SELECT 
                    abc.id,
                    abc.name,
                    abc.jp_name,
                    abc.title,
                    abc.description,
                    abc.big_category_id,
                    abbc.jp_name as big_cate_name,
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_un,
                    REPLACE(LOWER(abbc.name),' ','-') as big_cate_slug
                FROM `aruaru_bbs_categorys` AS abc
                LEFT JOIN `aruaru_bbs_big_categorys` AS abbc ON abc.big_category_id = abbc.id
                WHERE abc.id = ?
                LIMIT 1
            ";
            $query = $this->db->query($sql, $cat_id);
            return $query->row_array(); 

        }


        /**
         * Retrieve all data in category main and its 
         * sub category (aruaru_bbs_big_categorys) 
         * @param unknown $cat_id : id of main category
         * @return multitype: array query result
         */
        function getCatesByBulletin($bid) {
            if (empty($bid)) return array();
            $sql = "
                SELECT 
                    abc.id,
                    abc.name,
                    abc.jp_name,
                    abc.title,
                    abc.big_category_id,
                    abbc.name AS big_cate_name,
                    abbc.jp_name AS big_cate_jp_name,
                    REPLACE(LOWER(abbc.name), ' ', '_') AS big_cate_slug
                FROM
                    aruaru_bbs_categorys AS abc
                LEFT JOIN
                    aruaru_bbs_big_categorys AS abbc ON abc.big_category_id = abbc.id
                INNER JOIN
                    aruaru_bbs_threads AS abt ON abt.cate_id = abc.id
                WHERE
                    abc.big_category_id = ?
                GROUP BY abc.id
                ORDER BY abc.priority ASC , abc.id
            ";

            $query = $this->db->query($sql, $bid);
            return $query->result_array();  
      
        }

        function getBigCateInfo($bid) {
//            $this->db->select('id, ad_type, ad_url, ad_image, ad_text, ad_interval');
            $this->db->from('aruaru_bbs_big_categorys');
            $this->db->where('id', $bid);
            $query = $this->db->get();
            $res = $query->row_array();
            return $res;
        }

        function getIsTopInfo() {
//            $this->db->select('id, ad_type, ad_url, ad_image, ad_text, ad_interval');
            $this->db->from('aruaru_bbs_is_top');
            $this->db->where('id', 1);
            $query = $this->db->get();
            $res = $query->row_array();
            return $res;
        }

    }
