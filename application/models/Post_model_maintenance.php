<?php
    class Post_model_maintenance extends CI_Model
    {
        function __construct() {
            parent::__construct();
        }

        /**
         * Get the latest message/post
         * @param number $offset
         * @param number $limit
         */
        function getLatestPosts_comment_time($offset = 0, $limit = 10) {
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.user_id as owner_id, 
                    abbc.jp_name as big_cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    max(abm.create_date) AS last_comment_time,
                    count(abm.id) as comment_count,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count
                 ".
                "FROM aruaru_bbs_threads AS abt ".
                "LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id ".
                "LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL ".
                "WHERE abt.publish = 1
                GROUP BY abt.id
                ORDER BY abt.create_date DESC
                "
//                ."LIMIT ".$limit."
//                OFFSET ".$offset
            ;


            $query = $this->db->query($sql);

//echo $this->db->last_query();

            return $query->result_array();
        }


        function getLatestPosts_comment_time_by_threads_id($offset = 0, $limit = 10, $pid = '') {

            $pid_sql = ($pid != '')? ' AND abt.id ='.$pid :'';

            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.user_id as owner_id, 
                    abbc.jp_name as big_cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    max(abm.create_date) AS last_comment_time,
                    count(abm.id) as comment_count,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count
                 ".
                "FROM aruaru_bbs_threads AS abt ".
                "LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id ".
                "LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL ".
                "WHERE abt.publish = 1 ".$pid_sql.
                " GROUP BY abt.id
                ORDER BY abt.create_date DESC
                "
//                ."LIMIT ".$limit."
//                OFFSET ".$offset
            ;


            $query = $this->db->query($sql);

echo $this->db->last_query();

            return $query->result_array();
        }


        function getLatestPostsBACK($offset = 0, $limit = 10) {
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.user_id as owner_id, 
                    abbc.jp_name as big_cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    max(abm.create_date) AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(abm.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    count(abm.id) as comment_count,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count
                 "."
                FROM aruaru_bbs_threads AS abt
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id 
                LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL
                WHERE abt.publish = 1
                GROUP BY abt.id
                ORDER BY abt.create_date DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;

            $query = $this->db->query($sql);
            return $query->result_array();
        }

        function getLatestPosts_comment_time_update($ar) {
            foreach ($ar as $key => $val) {
                $data = array(
                               'last_comment_time' => $val['last_comment_time'],
                               'category_like_count' => $val['like_count']
                            );
                $this->db->where('id', $val['id']);
                $this->db->update('aruaru_bbs_threads', $data); 
            }

//echo $this->db->last_query();

        return true;
        }


    }
