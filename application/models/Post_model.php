<?php
    class Post_model extends CI_Model
    {
        function __construct() {
            parent::__construct();
        }
        
		/**
		 * Select specific threads 
		 * @param string $id
		 */
        function getPost($id = null) {
            $sql = 'SELECT * FROM aruaru_bbs_threads WHERE id = ? AND publish = 1';
            $query = $this->db->query($sql, $id);
            return $query->row_array();
        }

        /**
         * Get post detail of the thread
         * @param string $id
         */
        function getPostDetail($id = null) {
            if(is_null($id)) return;
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.message, 
                    abt.up_image,
                    abt.create_date, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.like_count,
                    abt.dislike_count,
                    abt.publish,
                    abt.user_id as owner_id, 
                    u.nick_name as owner_nickname,
                    DATE_FORMAT(abt.create_date,'%Y/%m/%d') as create_date_fdate,
                    DATE_FORMAT(abt.create_date,'%H:%i') as create_date_ftime,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, abt.create_date, NOW()) >= 24) THEN DATE_FORMAT(abt.create_date,'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, abt.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, abt.create_date, NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, abt.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, abt.create_date, NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    max(abm.create_date) AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(abm.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as comment_time_ago,
                    abbc.jp_name as big_cate_name, 
                    abc.jp_name as cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    count(abm.id) as comment_count,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as comment_like_count
                 "."
                FROM aruaru_bbs_threads AS abt
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id 
                LEFT JOIN aruaru_bbs_categorys AS abc ON abt.cate_id = abc.id
                LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.parent_id IS NULL AND abm.publish = 1
                LEFT JOIN users AS u ON abt.user_id = u.id
                WHERE abt.id = ?
                    AND abt.publish = 1
                LIMIT 1
            ";
            $query = $this->db->query($sql, $id);
            return $query->row_array();

        }

        /**
         * Get the latest message/post
         * @param number $offset
         * @param number $limit
         */
        function getLatestPostsV2($offset = 0, $limit = 10) {
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.user_id as owner_id,
                    abt.last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, abt.last_comment_time, NOW()) >= 24) THEN DATE_FORMAT(abt.last_comment_time,'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, abt.last_comment_time, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, abt.last_comment_time, NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, abt.last_comment_time, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, abt.last_comment_time, NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    (IFNULL(sum(abt.category_like_count),0) ) as like_count
                 ".
                "FROM aruaru_bbs_threads AS abt ".
//                "LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id ".
                "WHERE abt.publish = 1
                GROUP BY abt.id
                ORDER BY abt.create_date DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;
            $query = $this->db->query($sql);
            return $query->result_array();
        }

        /**
         * Get the latest message/post
         * @param number $offset
         * @param number $limit
         */
        function getLatestPosts($offset = 0, $limit = 10) {
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.user_id as owner_id, 
                    abbc.jp_name as big_cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    max(abm.create_date) AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(abm.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    count(abm.id) as comment_count,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count
                 "."
                FROM aruaru_bbs_threads AS abt
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id 
                LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL
                WHERE abt.publish = 1
                GROUP BY abt.id
                ORDER BY abt.create_date DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;

            $query = $this->db->query($sql);
            return $query->result_array();
        }

        /* 取得しておく */
        function getLatestPosts_comment_time() {
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.user_id as owner_id, 
                    abbc.jp_name as big_cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    max(abm.create_date) AS last_comment_time,
                    count(abm.id) as comment_count,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count
                 ".
                "FROM aruaru_bbs_threads AS abt ".
                "LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id ".
                "LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL ".
                "WHERE abt.publish = 1
                GROUP BY abt.id
                ORDER BY abt.create_date DESC
                ";
            $query = $this->db->query($sql);
            return $query->result_array();
        }

        function getLatestPosts_comment_time_by_threads_id($offset = 0, $limit = 10, $pid = '') {
            $pid_sql = ($pid != '')? ' AND abt.id ='.$pid :'';
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.user_id as owner_id, 
                    abbc.jp_name as big_cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    max(abm.create_date) AS last_comment_time,
                    count(abm.id) as comment_count,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count
                 ".
                "FROM aruaru_bbs_threads AS abt ".
                "LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id ".
                "LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL ".
                "WHERE abt.publish = 1 ".$pid_sql.
                " GROUP BY abt.id
                ORDER BY abt.create_date DESC
                ";
            $query = $this->db->query($sql);
            return $query->result_array();
        }

        /* コメント時間、あるある数更新 */
        function getLatestPosts_comment_time_update($ar) {
            foreach ($ar as $key => $val) {
                $data = array(
                               'last_comment_time' => $val['last_comment_time'],
                               'category_like_count' => $val['like_count']
                            );
                $this->db->where('id', $val['id']);
                $this->db->update('aruaru_bbs_threads', $data); 
            }
            return true;
        }


        /**
         * count users post in threads
         * @param number $user_id
         */
        public function getUserPostsCount($user_id = 0) {
            if ($user_id == 0) {
                return;
            }
            $this->db->where('user_id', $user_id);
            $this->db->where('publish', 1);
            $query = $this->db->get('aruaru_bbs_threads');
            return $query->num_rows();
        }
        
        /**
         * Get user comment/post
         * @param number $user_id
         * @param number $offset
         * @param number $limit
         */
        function getUserPosts($user_id = 0, $offset = 0, $limit = 10) {
            if ($user_id == 0) {
                return;
            }
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.message, 
                    abt.create_date, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.user_id as owner_id, 
                    abbc.jp_name as big_cate_name,
                    abc.jp_name as cate_name,                    
                    DATE_FORMAT(abt.create_date,'%Y/%m/%d') as create_date_fdate,
                    DATE_FORMAT(abt.create_date,'%H:%i') as create_date_ftime,
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, abm.create_date, NOW()) >= 24) THEN DATE_FORMAT(abm.create_date,'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, abm.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, abm.create_date, NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, abm.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, abm.create_date, NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    count(abm.id) as comment_count
                 "."
                FROM aruaru_bbs_threads AS abt
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id
                LEFT JOIN aruaru_bbs_categorys AS abc ON abt.cate_id = abc.id 
                LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1
                WHERE abt.user_id = {$user_id}
                    AND abt.publish = 1
                GROUP BY abt.id
                ORDER BY abt.create_date DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;
            $query = $this->db->query($sql);
            return $query->result_array();
        }

        /**
         * Get latest post by comments
         * @param number $offset
         * @param number $limit
         */
        function getLatestPostsByComments($offset = 0, $limit = 10) {
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.user_id as owner_id, 
                    abbc.jp_name as big_cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    max(abm.create_date) AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(abm.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    count(abm.id) as comment_count,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count
                 "."
                FROM aruaru_bbs_threads AS abt
                INNER JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id 
                WHERE abt.publish = 1
                GROUP BY abm.thread_id
                ORDER BY last_comment_time DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;

            $query = $this->db->query($sql);
            return $query->result_array();
        }

        /**
         * Retrieve Post by comments category or thread
         * @param unknown $catid
         * @param number $offset
         * @param number $limit
         * @return multitype:
         */
        function getLatestPostsByCatComment($catid, $offset = 0, $limit = 10) {
            if (is_null($catid)) return array();
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    max(abm.create_date) AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(abm.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    count(abm.id) as comment_count,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count,
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    abbc.jp_name as big_cate_name
                 "."
                FROM aruaru_bbs_threads AS abt
                INNER JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id
                WHERE abt.cate_id = ?
                    AND abt.publish = 1
                GROUP BY abm.thread_id
                ORDER BY last_comment_time DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;

            $query = $this->db->query($sql, $catid);
            return $query->result_array();
        }

        /**
         * Get latest thread/post by category
         * @param unknown $catid
         * @param number $offset
         * @param number $limit
         * @return multitype:
         */
        function getLatestPostsByCat($catid, $offset = 0, $limit = 10) {
            if (is_null($catid)) return array();
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    max(abm.create_date) AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(abm.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    count(abm.id) as comment_count,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count,                    
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    abbc.jp_name as big_cate_name
                 "."
                FROM aruaru_bbs_threads AS abt
                LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id
                WHERE abt.cate_id = ?
                    AND abt.publish = 1
                GROUP BY abt.id
                ORDER BY abt.create_date DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;

            $query = $this->db->query($sql, $catid);
            return $query->result_array();
        }

        /** 
         * Retreive numbers of post by category
         * @param unknown $catid
         * @return number else zero if empty
         */
        function getCatPostCount($catid) {
            if(empty($catid)) return 0;
            $sql = "
                SELECT  count(abt.id) as count
                FROM aruaru_bbs_threads AS abt
                WHERE abt.cate_id = ?
                    AND abt.publish = 1
            ";
            $query = $this->db->query($sql, $catid);
            $data = $query->row_array();
            return $data['count'];
        }

        /**
         * Display number of comment by category thread
         * @param unknown $catid
         * @return Ambigous <number, unknown>
         */
        function getCatPostByCommentCount($catid) {
            if(empty($catid)) return 0;

            $sql = "
            SELECT count(abt.id) as count
            FROM aruaru_bbs_threads AS abt
            INNER JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1
            WHERE abt.cate_id = ?
                AND abt.publish = 1
            GROUP BY abm.thread_id
            ";

            $query = $this->db->query($sql, $catid);
            return $query->num_rows();
        }

        /**
         * Retrieve the latest post category Comment
         * @param unknown $bid: main category id
         * @param number $offset
         * @param number $limit
         * @return multitype:
         */
        function getLatestPostsByBulletinComment($bid, $offset = 0, $limit = 10) {
            if (is_null($bid)) return array(); 
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    max(abm.create_date) AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(abm.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    abbc.jp_name as big_cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    count(abm.id) as comment_count,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count                    
                 "."
                FROM aruaru_bbs_threads AS abt
                INNER JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id 
                WHERE abt.big_cate_id = ?
                    AND abt.publish = 1
                GROUP BY abm.thread_id
                ORDER BY last_comment_time DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;

            $query = $this->db->query($sql, $bid);
            return $query->result_array();
        }

        function getLatestPostsByBulletinCommentV2($bid, $offset = 0, $limit = 10) {
            if (is_null($bid)) return array(); 
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.create_date, 
                    max(abm.create_date) AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(abm.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    count(abm.id) as comment_count,
                    (IFNULL(abt.category_like_count,0) + IFNULL(abt.like_count,0)) as like_count
                 "."
                FROM aruaru_bbs_threads AS abt
                INNER JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL
                WHERE abt.big_cate_id = ?
                    AND abt.publish = 1
                GROUP BY abm.thread_id
                ORDER BY last_comment_time DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;

            $query = $this->db->query($sql, $bid);
            return $query->result_array();
        }

        /**
         * Get latest post by main category
         * @param unknown $bid
         * @param number $offset
         * @param number $limit
         * @return multitype: array query result
         */

        function getLatestPostsByBulletin($bid, $offset = 0, $limit = 10) {
            if (is_null($bid)) return array();
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    max(abm.create_date) AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(abm.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    abbc.jp_name as big_cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    count(abm.id) as comment_count,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count
                 "."
                FROM aruaru_bbs_threads AS abt
                LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id 
                WHERE abt.big_cate_id = ?
                    AND abt.publish = 1
                GROUP BY abt.id
                ORDER BY abt.create_date DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;

            $query = $this->db->query($sql, $bid);
            return $query->result_array();
        }

        function getLatestPostsByBulletinV2($bid, $offset = 0, $limit = 10) {
            if (is_null($bid)) return array();
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.create_date, 
                    max(abm.create_date) AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(abm.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,".
                    /*abbc.jp_name as big_cate_name,
                    "REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,*/
                    "count(abm.id) as comment_count
                   ,(IFNULL(abt.category_like_count,0) + IFNULL(abt.like_count,0)) as like_count".
/*                   ,(IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count */
                "
                FROM aruaru_bbs_threads AS abt
                LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL ".
                /*LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id*/
                "WHERE abt.big_cate_id = ?
                    AND abt.publish = 1
                GROUP BY abt.id
                ORDER BY abt.create_date DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;

            $query = $this->db->query($sql, $bid);
            return $query->result_array();
        }

        /**
         * Get post detail of the thread
         * @param string $id
         */
        function getPostCommentCount($id = null) {
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    count(abm.id) as comment_count
                 "."
                FROM aruaru_bbs_threads AS abt
                LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.parent_id IS NULL AND abm.publish = 1
                WHERE abt.id = ?
                    AND abt.publish = 1
            ";
            $query = $this->db->query($sql, $id);
            return $query->row_array();

        }
		
        /**
         * Retrieve the numbers of comment by post category main
         * @param unknown $bid
         * @return count
         */
        function getPostsByBulletinCommentCount($bid) {
            if (empty($bid)) return 0;
            $sql = "
            SELECT count(abt.id) as count 
            FROM aruaru_bbs_threads AS abt
            INNER JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1
            WHERE abt.big_cate_id = ?
                AND abt.publish = 1
            GROUP BY abm.thread_id
            ";
            $query = $this->db->query($sql, $bid);
            return $query->num_rows();
        }

		/**
		 * Display number of post in category
		 * @param unknown $bid
		 * @return count
		 */
        function getBulletinPostCount($bid) {
            if (!empty($bid)) {
                $sql = "
                    SELECT  count(abt.id) as count
                    FROM aruaru_bbs_threads AS abt
                    WHERE abt.cate_id = ?
                        AND abt.publish = 1
                ";

                $query = $this->db->query($sql, $bid);
                $post =  $query->row_array();
            } 
            return (isset($post['count'])) ? $post['count'] : 0;
        }

        /**
         * Get user's latest post
         * @param number $user_id
         * @param number $offset
         * @param number $limit
         */
        function getUsersLatestPosts($user_id, $offset = 0, $limit = 10) {
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.user_id as owner_id, 
                    abbc.jp_name as big_cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    max(abm.create_date) AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(abm.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    count(abm.id) as comment_count,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count
                 "."
                FROM aruaru_bbs_threads AS abt
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id 
                LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 and abm.parent_id IS NULL
                WHERE abt.user_id = ?
                AND abt.publish = 1
                GROUP BY abt.id
                ORDER BY abt.create_date DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;

            $query = $this->db->query($sql,$user_id);
            return $query->result_array();
        }

        /**
         * Get user's latest post count
         * @param number $user_id
         */
        function getUsersLatestPostCount($user_id) {
            if(empty($user_id)) return 0;
            $sql = "
                SELECT  count(abt.id) as count
                FROM aruaru_bbs_threads AS abt
                WHERE abt.user_id = ?
                    AND abt.publish = 1
            ";
            $query = $this->db->query($sql, $user_id);
            $data = $query->row_array();
            return $data['count'];
        }

        /**
         * Get user's latest post by comments
         * @param number $user_id
         * @param number $offset
         * @param number $limit
         */
        function getUsersLatestPostsByComments($user_id, $offset = 0, $limit = 10) {
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.create_date, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.user_id as owner_id, 
                    abbc.jp_name as big_cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    max(abm.create_date) AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(abm.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    count(abm.id) as comment_count,
                    abm.id as comment_id,
                    (IFNULL(sum(abm.like_count),0) + IFNULL(abt.like_count,0)) as like_count
                 "."
                FROM aruaru_bbs_threads AS abt
                INNER JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1 AND abm.parent_id IS NULL
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id 
                WHERE abm.user_id = ?
                AND abt.publish = 1
                GROUP BY abm.thread_id
                ORDER BY last_comment_time DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;

            $query = $this->db->query($sql,$user_id);
            return $query->result_array();
        }

        /** 
         * Retreive numbers of post by user
         * @param unknown $user_id
         * @return number else zero if empty
         */
        function getUsersLatestPostsByCommentCount($user_id) {
            if(empty($user_id)) return 0;
            $sql = "
                SELECT  count(abt.id) as count
                FROM aruaru_bbs_threads AS abt
                INNER JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1
                WHERE abm.user_id = ?
                AND abt.publish = 1
                GROUP BY abm.thread_id
            ";
            $query = $this->db->query($sql, $user_id);
            return $query->num_rows();
        }

        /**
         * Check if posts by user
         * @param string $user_id
         * @param string $id
         */
        function isUsersPost($user_id, $id) {
            $this->db->select('id');
            $this->db->where('user_id', $user_id);
            $this->db->where_in('id', $id);
            $query = $this->db->get('aruaru_bbs_threads');
            return $query->result_array();
        }

		/**
		 * Insert new thread in aruaru_bbs_threads
		 * @param unknown $data: inserted value
		 */
        function insert($data) {
            $data['create_date'] = date('Y-m-d H:i:s');
            $data['board_id'] = BOARD_ID;
            $this->db->insert('aruaru_bbs_threads', $data);
            return  $this->db->insert_id();
        }


        function updateData($id, $data) {
            $this->db->where('id', $id);
            return $this->db->update('aruaru_bbs_threads', $data);
        }

      	/**
         * Updates the likes of the comments
         * @param unknown $id: id of the message
         * @param unknown $react :(like_discount, dislike_count field)
         * @return boolean: check if successfully update 
         */
        function react($id, $react) {
            $this->db->set($react.'_count', '`'.$react.'_count`+1', FALSE);
            $this->db->where('id', $id);
            return $this->db->update('aruaru_bbs_threads');
        }
        
        /**
         * Display Multiple posts
         * @param string $post_ids: thread_id 
         * @param number $user_id: user_id
         */
        function deleteMultiplePosts($post_ids,$user_id) {

            $this->db->set('publish', 3);
            $this->db->where('user_id', $user_id);
            $this->db->where_in('id', $post_ids);
            $this->db->update('aruaru_bbs_threads');

            # deduct points to post owner target=1
            #deduct all aruaru points for the comments target=4 
            $this->load->model('Point_model', 'point');
            $cond = array(
                'target' => 1, 
                'user_id' => $user_id,
                'thread_id' => $post_ids,
                'bonus_requested_flag' => 0
            );
            $rows = $this->point->getAruaruPoints($post_ids, $user_id, array(1));
            $result = $this->point->deduct($cond);
            if (!empty($rows)) {
                foreach ($rows as $key => $value) {
                     $this->point->updateScoutBonus($value['user_id'], $value['point'] , '投稿ボーナスを引く', 1);
                }
            }
            
            #delete comments and deduct points to all commenters under these posts
            $this->load->model('Comment_model', 'comment');
            $this->comment->deleteMultipleComments($post_ids,null);
        }

        public function check_ip($id = null, $pid = null, $cid = null, $ip = null) {
            $this->db->select('create_date');
            $this->db->where('comment_id', $cid);
            $this->db->where('thread_id', $pid);
            $this->db->where('target', 4);
            $this->db->where('create_ip', $ip);
            //$this->db->where('create_date <= DATE_SUB(NOW(), INTERVAL 24 HOUR)');
            $this->db->where("DATE_FORMAT(`create_date`, '%Y/%m/%d') = DATE_FORMAT(NOW(), '%Y/%m/%d')");
            $this->db->order_by('create_date', 'desc');
            $this->db->from('aruaru_bbs_points');
            $query =  $this->db->get();
            if ($query->num_rows() > 0) {
                return false;
            } else {
                return true;
            }
        }

    }
