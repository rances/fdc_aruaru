<?php
    class Bulletin_model extends CI_Model
    {
        function __construct() {
            parent::__construct();
        }
        
        /**
         * Get all Category in aruaru_bbs_big_categorys
         * with the specifics $colums
         * @param string $columns ex: "*"  is to get column field name, "name,title"
         */
        function getRecords($columns = "*") {
            $sql = "
                SELECT " .
                $columns . "
                FROM `aruaru_bbs_big_categorys`
                WHERE board_id IS NOT NULL
                AND jp_name <> ''
                ORDER BY `priority` ASC, `id`
            ";
            $query = $this->db->query($sql);
            return $query->result_array();            
        }

        /*
         * GET main category
         */
        public function get_big_category() {
                $query = $this->db->get('aruaru_bbs_big_categorys');
                return $query->result();
        }

        public function get_big_category_ar() {
                $query = $this->db->get('aruaru_bbs_big_categorys');
                return $query->result_array();
        }

        /**
         * Get category by id
         * @param string $id
         * @return multitype:
         */
        function getRowById($id = null) {
            if (empty($id)) return array();
            $sql = '
                SELECT * 
                FROM `aruaru_bbs_big_categorys`
                WHERE `id` = ?
                LIMIT 1
            ';
            $query = $this->db->query($sql, $id);
            return $query->row_array();   

        }
        
        /**
         * get total rows for every query execute
         * note:
         * 1. add sql_calc_found_rows for every sql query
         * 2. not applicable for default Codeigniter syntx ,(Please search for alternative)
         */
        public function get_total_rows() {
            $sql = "SELECT FOUND_ROWS() as total";
            $query = $this->db->query($sql);
            return $query->row_array();  
        }
        
		/**
		 * Get Category and Sub Category
		 * title and descriptions
		 */
        public function getCategoryMeta() {
           $this->db->select(
                '
                 abbc.id,
                 abbc.title as b_title,
                 abbc.description as b_desc,
                 abc.id as sub_id,
                 abc.title as sub_title,
                 abc.description as sub_description,
                '
            );
           $this->db->from('aruaru_bbs_big_categorys as abbc');
           $this->db->join('aruaru_bbs_categorys as abc','abc.big_category_id = abbc.id','LEFT');
           $query = $this->db->get();
           return $query->result_array();
        }

    }