<?php
class Bonus_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * @name 	getUserBonusData
     * @todo 	get bonus data of a user
     * @param 	user_id
     */
    public function getUserBonusData($user_id = 0) {
        if ($user_id == 0) return;
        $sql = '
                SELECT 
                    CASE WHEN count(1) > 0 THEN SUM(CASE WHEN target = 1 THEN point ELSE 0 END) ELSE 0 END thread_points,
                    CASE WHEN count(1) > 0 THEN SUM(CASE WHEN target = 2 THEN point ELSE 0 END) ELSE 0 END comment_points,
                    CASE WHEN count(1) > 0 THEN SUM(CASE WHEN target = 3 THEN point ELSE 0 END) ELSE 0 END thread_like_points,
                    CASE WHEN count(1) > 0 THEN SUM(CASE WHEN target = 4 THEN point ELSE 0 END) ELSE 0 END comment_like_points
                FROM aruaru_bbs_points 
                WHERE 
                    validity = 1 and 
                    user_id = ? and bonus_requested_flag = 0
               ';
        $query = $this->db->query($sql, $user_id);
        return $query->row_array();
    }
}

?>
