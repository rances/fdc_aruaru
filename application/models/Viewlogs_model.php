<?php
    class Viewlogs_model extends CI_Model
    {
        function __construct() {
            parent::__construct();
        }

        function get3daysLogs($params = array(), $offset = 0, $limit = 10)
        {
            $cond = (empty($params)) ? '':$params['row']." = ". $params['value']." AND";

            $sql = "
                SELECT 
                    avbl.thread_id as id,
                    abt.title,
                    count(avbl.thread_id) as log_count, 
                    avbl.create_date
                FROM aruaru_bbs_view_logs AS avbl 
                INNER JOIN aruaru_bbs_threads AS abt ON avbl.thread_id = abt.id AND abt.publish = 1
                WHERE ".$cond."
                avbl.create_date BETWEEN CURDATE() - INTERVAL 2 DAY AND CURDATE() + INTERVAL 1 DAY
                GROUP BY avbl.thread_id
                ORDER BY log_count DESC, avbl.create_date DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;
            $query = $this->db->query($sql);
            return $query->result_array();
        }

        function get3daysLogsHome($params = array(), $offset = 0, $limit = 10)
        {
            $cond = (empty($params)) ? '':$params['row']." = ". $params['value']." AND";

            $sql = "
                SELECT 
                    avbl.thread_id as id,
                    abt.title,
                    abt.category_like_count as like_count,
                    count(avbl.thread_id) as log_count, 
                    avbl.create_date,
                    abt.last_comment_time AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, abt.last_comment_time, NOW()) >= 24) THEN DATE_FORMAT(abt.last_comment_time,'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, abt.last_comment_time, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, abt.last_comment_time, NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, abt.last_comment_time, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, abt.last_comment_time, NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as comment_time_ago,
                    abbc.jp_name as big_cate_name, 
                    abc.jp_name as cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug
                FROM aruaru_bbs_view_logs AS avbl 
                INNER JOIN aruaru_bbs_threads AS abt ON avbl.thread_id = abt.id AND abt.publish = 1
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id 
                LEFT JOIN aruaru_bbs_categorys AS abc ON abt.cate_id = abc.id
                WHERE ".$cond."
                avbl.create_date BETWEEN CURDATE() - INTERVAL 2 DAY AND CURDATE() + INTERVAL 1 DAY
                GROUP BY avbl.thread_id
                ORDER BY log_count DESC, avbl.create_date DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;
            $query = $this->db->query($sql);
            return $query->result_array();
        }


        function get3daysLogsV3($params = array(), $offset = 0, $limit = 10)
        {
            $cond = (empty($params)) ? '':$params['row']." = ". $params['value']." AND";

            $sql = "
                SELECT 
                    avbl.thread_id as id,
                    abt.title,
                    abt.category_like_count as like_count,
                    count(avbl.thread_id) as log_count, 
                    avbl.create_date,
                    abt.last_comment_time AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, abt.last_comment_time, NOW()) >= 24) THEN DATE_FORMAT(abt.last_comment_time,'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, abt.last_comment_time, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, abt.last_comment_time, NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, abt.last_comment_time, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, abt.last_comment_time, NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    abbc.jp_name as big_cate_name, 
                    abc.jp_name as cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug
                FROM aruaru_bbs_view_logs AS avbl 
                INNER JOIN aruaru_bbs_threads AS abt ON avbl.thread_id = abt.id AND abt.publish = 1
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id 
                LEFT JOIN aruaru_bbs_categorys AS abc ON abt.cate_id = abc.id
                WHERE ".$cond."
                avbl.create_date BETWEEN CURDATE() - INTERVAL 2 DAY AND CURDATE() + INTERVAL 1 DAY
                GROUP BY avbl.thread_id
                ORDER BY log_count DESC, avbl.create_date DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;
            $query = $this->db->query($sql);
            return $query->result_array();
        }



        function get3daysLogsV2($params = array(), $offset = 0, $limit = 10)
        {
            $cond = (empty($params)) ? '':$params['row']." = ". $params['value']." AND";

            $sql = "
                SELECT 
                    avbl.thread_id as id,
                    abt.title,
                    abt.like_count,
                    count(avbl.thread_id) as log_count, 
                    avbl.create_date,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(avbl.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(avbl.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(avbl.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(avbl.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(avbl.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(avbl.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    max(abm.create_date) AS last_comment_time,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()) >= 24) THEN DATE_FORMAT(max(abm.create_date),'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, max(abm.create_date), NOW()),' 時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, max(abm.create_date), NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, max(abm.create_date), NOW()),' 分前')
                            ELSE 'ちょうど今'
                        END
                    ) as comment_time_ago,
                    abbc.jp_name as big_cate_name, 
                    abc.jp_name as cate_name, 
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug
                FROM aruaru_bbs_view_logs AS avbl 
                INNER JOIN aruaru_bbs_threads AS abt ON avbl.thread_id = abt.id AND abt.publish = 1
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id 
                LEFT JOIN aruaru_bbs_categorys AS abc ON abt.cate_id = abc.id
                LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.parent_id IS NULL AND abm.publish = 1
                WHERE ".$cond."
                avbl.create_date BETWEEN CURDATE() - INTERVAL 2 DAY AND CURDATE() + INTERVAL 1 DAY
                GROUP BY avbl.thread_id
                ORDER BY log_count DESC, avbl.create_date DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;
            $query = $this->db->query($sql);
            return $query->result_array();
        }

        function insert($data)
        {
            $data['create_date'] = date('Y-m-d H:i:s');
            $data['create_ip'] = $_SERVER['REMOTE_ADDR'];
            if(!empty($this->user['id']))
                $data[' user_id'] = $this->user['id'];
            $this->db->insert('aruaru_bbs_view_logs', $data);
            return  $this->db->insert_id();
        }

        function alreadyAccessToday($id)
        {

            $data = array($id, $_SERVER['REMOTE_ADDR']);
            $user = (!empty($this->user['id'])) ? '= '.$this->user['id']:'IS NULL';
            $sql = "
                SELECT 
                    avbl.thread_id as id
                FROM aruaru_bbs_view_logs AS avbl 
                WHERE avbl.thread_id = ?
                AND avbl.user_id ".$user."
                AND avbl.create_ip = ?
                AND DATE(avbl.create_date) = CURDATE()
            ";
            $query = $this->db->query($sql, $data);
            $row =  $query->row_array();
            return (empty($row))? true : false;
        }

    }
