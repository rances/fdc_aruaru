<?php
class Thread_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getThreadinfo($id) {
        $sql = "SELECT big_cate_id, cate_id, title FROM aruaru_bbs_threads WHERE id = {$id}";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getThreadinfoTw($id) {
        $sql = "SELECT id, big_cate_id, cate_id, title, message, image_up_flag, latest_media_ids FROM aruaru_bbs_threads WHERE id = {$id}";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getThreadinfoAdvert($id) {
        $sql = "SELECT id, big_cate_id, cate_id FROM aruaru_bbs_threads WHERE id = {$id}";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function setThreadinfo($id, $ar) {
        $this->db->where('id', $id);
        $this->db->update('aruaru_bbs_threads', $ar); 
//echo $this->db->last_query();
		return true;
    }

    public function catThreadinfoTitle($id, $cattitle, $cat_flag = 'LEFT') {
        $sql = "SELECT big_cate_id, cate_id, title, image_up_flag FROM aruaru_bbs_threads WHERE id = {$id}";
        $query = $this->db->query($sql);
        $arr = $query->row_array();

        if($arr['image_up_flag'] == 0){
			if($cat_flag == 'LEFT'){
	            $data = array(
	               'title' => $cattitle.$arr['title'],
	               'image_up_flag' => 1
	            );
			}else{
	            $data = array(
	               'title' => $arr['title'].$cattitle,
	               'image_up_flag' => 1
	            );

			}
	        $this->db->where('id', $id);
	        $this->db->update('aruaru_bbs_threads', $data); 
			return true;
        }
//echo $this->db->last_query();
		return false;
    }


}

?>
