<?php
class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * @name 	get_users
     * @todo 	get data users
     * @param 	id
     */
    public function get_users($id = null) {
        $sql = 'SELECT * FROM users WHERE display_flag = 1 and id = ?';
        $query = $this->db->query($sql, $id);
        $user = $query->row_array();

        if($user){
            $user['nick_name'] = (empty($user['nick_name'])) ? 'とくめい' : $user['nick_name'];
        }
        return $user;
    }

    /**
     * @name    getsPoint
     * @todo    check if user is allowed to get point 
     * @todo    Macherie mobile, Maquia users only
     * @param   id => user id
     */
    public function getsPoint($id) {
        if(empty($id)) return false;
        $sql = "
            SELECT 
                id,  
                user_from_site 
            FROM users 
            WHERE 
                display_flag = 1 
                AND id = ? 
                AND user_from_site in (1,2)";
        $query = $this->db->query($sql, $id);
        $user = $query->row_array();
        return (empty($user['id']))? false : true;
    }

}

?>
