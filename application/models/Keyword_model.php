<?php
class Keyword_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /* キーワード検索 */
    public function get_keyword_search_back($arr)
    {
//        $this->db->select(    );
        $this->db->from('aruaru_bbs_threads');

        $sql = "(";
        foreach ($arr as $key => $val) {
            $sql .= ($key > 0)? " OR ":"";
            $sql .= " `title` LIKE ";
            $sql .= " '%".$val."%' ";
        }
        foreach ($arr as $key => $val) {
            $sql .= " OR ";
            $sql .= "  `message` LIKE ";
            $sql .= " '%".$val."%' ";
        }
        $sql .= ")";
        $this->db->where($sql);
//        $this->db->where('finish_flag', 1);
//        $this->db->order_by('finish_date', 'DESC');
        $this->db->order_by('create_date', 'DESC');
        $query = $this->db->get();
        $res = $query->result_array();
//echo $this->db->last_query();
       return $res;
    }

    /* キーワード検索 */
    public function get_keyword_search($arr)
    {
        $this->db->from('aruaru_bbs_threads AS bt');
        $sql = "(";
        foreach ($arr as $key => $val) {
            $sql .= ($key > 0)? " OR ":"";
            $sql .= " `bt.title` LIKE ";
            $sql .= " '%".$val."%' ";
        }
        foreach ($arr as $key => $val) {
            $sql .= " OR ";
            $sql .= "  `bt.message` LIKE ";
            $sql .= " '%".$val."%' ";
        }
        $sql .= ")";
        $this->db->where($sql);
        $this->db->order_by('bt.create_date', 'DESC');
        $query = $this->db->get();
        $res = $query->result_array();
       return $res;
    }
}

?>
