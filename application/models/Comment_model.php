<?php
    class Comment_model extends CI_Model
    {
        function __construct() {
            parent::__construct();
        }
		
        /**
         * Retrieve all data in aruaru_bbs_messages by its id
         * @param string $id
         */
        function getComment($id = null) 
        {
            $sql = "
                SELECT 
                    abm.*, IFNULL(abmr.rank, 0) AS rank
                FROM
                    aruaru_bbs_messages AS abm
                        LEFT JOIN
                    aruaru_bbs_messages_rank AS abmr ON abm.id = abmr.id
                WHERE
                    abm.id = ? AND abm.publish = 1
            ";
            $query = $this->db->query($sql, $id);
            return $query->row_array();
        }

        /**
         * Retrieve all data in aruaru_bbs_messages by its id
         * @param string $id
         */
        function getCommentCount($id = null) 
        {
            $sql = "SELECT abm.* ";
            $sql.= "FROM aruaru_bbs_messages AS abm ";
            $sql.= "WHERE abm.id = ? AND abm.publish = 1";
            $query = $this->db->query($sql, $id);
            $res = $query->num_rows();
            return $res;
        }



        /**
         * Get message by parent
         * @param string $id
         */
        function getCommentByParent($id = null) 
        {
            $sql = "
            SELECT 
                abm.id,
                abm.thread_id as post_id,
                abm.message,
                abm.user_id as owner_id,
                abm.parent_id,
                abm.like_count,
                abm.dislike_count,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, abm.create_date, NOW()) >= 24) THEN DATE_FORMAT(abm.create_date,'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, abm.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, abm.create_date, NOW()),'時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, abm.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, abm.create_date, NOW()),'分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                COALESCE(u.nick_name, 'とくめい') as owner_nick_name
            FROM aruaru_bbs_messages AS abm
            LEFT JOIN users AS u ON abm.user_id = u.id
            WHERE abm.parent_id = ? 
                AND abm.publish = 1
            ";
            $query = $this->db->query($sql, $id);
            return $query->row_array();
        }

        /**
         * Get message by parent
         * @param string $id
         */
        function getCommentByParentAr($id = null) 
        {
            $sql = "
            SELECT 
                abm.id,
                abm.thread_id as post_id,
                abm.message,
                abm.user_id as owner_id,
                abm.parent_id,
                abm.like_count,
                abm.dislike_count,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, abm.create_date, NOW()) >= 24) THEN DATE_FORMAT(abm.create_date,'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, abm.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, abm.create_date, NOW()),'時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, abm.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, abm.create_date, NOW()),'分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                COALESCE(u.nick_name, 'とくめい') as owner_nick_name
            FROM aruaru_bbs_messages AS abm
            LEFT JOIN users AS u ON abm.user_id = u.id
            WHERE abm.parent_id = ? 
                AND abm.publish = 1
            ";
            $query = $this->db->query($sql, $id);
            return $query->result_array();
        }

        /**
         * Display the detail of the comment
         * @param string $id
         */
        function getCommentDetail($id = null) 
        {
            $sql = "
            SELECT 
                abm.id,
                abm.thread_id as post_id,
                abm.message,
                abm.user_id as owner_id,
                abm.parent_id,
                abm.up_image,
                abm.like_count,
                abm.dislike_count,
                DATE_FORMAT(abm.create_date,'%Y/%m/%d %H:%i') as posted_date,
                (   CASE 
                        WHEN (TIMESTAMPDIFF(HOUR, abm.create_date, NOW()) >= 24) THEN DATE_FORMAT(abm.create_date,'%Y/%m/%d %H:%i')
                        WHEN (TIMESTAMPDIFF(MINUTE, abm.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, abm.create_date, NOW()),'時間前')
                        WHEN (TIMESTAMPDIFF(SECOND, abm.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, abm.create_date, NOW()),'分前')
                        ELSE 'ちょうど今'
                    END
                ) as time_ago,
                COALESCE(u.nick_name, 'とくめい') as owner_nick_name,
                IFNULL(abmr.rank,0) as rank
            FROM aruaru_bbs_messages AS abm
            LEFT JOIN users AS u ON abm.user_id = u.id
            LEFT JOIN aruaru_bbs_messages_rank AS abmr ON abm.id = abmr.id
            WHERE abm.id = ? 
            AND abm.publish = 1
            ";
            $query = $this->db->query($sql, $id);
            return $query->row_array();
        }

		/**
		 * Retrieve latest message
		 * @param string $id : thread id of the message
		 * @param number $offset
		 * @param number $limit
		 * @return multitype:
		 */
        function getLatestComments($id = null, $offset = 0, $limit = 10) 
        {
            if (is_null($id)) return array();
            $sql = "
            SELECT 
                abm.id,
                abm.thread_id as post_id,
                abm.message,
                abm.user_id as owner_id,
                abm.parent_id,
                abm.up_image,
                abm.like_count,
                abm.dislike_count,
                DATE_FORMAT(abm.create_date,'%Y/%m/%d %H:%i') as posted_date,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, abm.create_date, NOW()) >= 24) THEN DATE_FORMAT(abm.create_date,'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, abm.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, abm.create_date, NOW()),'時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, abm.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, abm.create_date, NOW()),'分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                COALESCE(u.nick_name, 'とくめい') as owner_nick_name,
                IFNULL(abmr.rank,0) as rank
            FROM aruaru_bbs_messages AS abm
            LEFT JOIN users AS u ON abm.user_id = u.id
            LEFT JOIN aruaru_bbs_messages_rank AS abmr ON abm.id = abmr.id
            WHERE abm.thread_id = ? 
                AND abm.parent_id IS NULL
                AND abm.publish = 1
            ORDER BY abm.create_date ASC
            LIMIT ".$limit."
            OFFSET ".$offset;

            $query = $this->db->query($sql, $id);
            return $query->result_array();
        }
        
        /**
         * Count the user comments and posts
         * @param number $user_id
         */
        function getUserCommentPostsCount($user_id = 0) 
        {
            if ($user_id == 0) {
                return;
            }
            $sql = "
                SELECT 
                    abt.id
                FROM aruaru_bbs_threads AS abt
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id 
                LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1
                WHERE abm.user_id = {$user_id}
                GROUP BY abt.id
                ORDER BY abt.create_date DESC
                "
            ;
            $query = $this->db->query($sql);
            return $query->num_rows();
        }
        
        /**
         * Get the current message of the users
         * @param number $user_id
         * @param number $offset
         * @param number $limit
         */
        function getUserComments($user_id = 0, $offset = 0, $limit = 10) 
        {
            if ($user_id == 0) {
                return;
            }
            $sql = "
                SELECT 
                    abt.id, 
                    abt.title, 
                    abt.message, 
                    abt.create_date, 
                    abt.big_cate_id, 
                    abt.cate_id, 
                    abt.user_id as owner_id, 
                    abbc.jp_name as big_cate_name,
                    abc.jp_name as cate_name,                    
                    abm.id as comment_id,
                    DATE_FORMAT(abt.create_date,'%Y/%m/%d') as create_date_fdate,
                    DATE_FORMAT(abt.create_date,'%H:%i') as create_date_ftime,
                    REPLACE(LOWER(abbc.name),' ','_') as big_cate_slug,
                    (   CASE 
                            WHEN (TIMESTAMPDIFF(MINUTE, abm.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, abm.create_date, NOW()),'時間前')
                            WHEN (TIMESTAMPDIFF(HOUR, abm.create_date, NOW()) >= 24) THEN DATE_FORMAT(abm.create_date,'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(SECOND, abm.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, abm.create_date, NOW()),'分前')
                            ELSE 'ちょうど今'
                        END
                    ) as time_ago,
                    count(abm.id) as comment_count
                 "."
                FROM aruaru_bbs_threads AS abt
                LEFT JOIN aruaru_bbs_big_categorys AS abbc ON abt.big_cate_id = abbc.id 
                LEFT JOIN aruaru_bbs_categorys AS abc ON abt.cate_id = abc.id 
                LEFT JOIN aruaru_bbs_messages AS abm ON abt.id = abm.thread_id AND abm.publish = 1
                WHERE abm.user_id = {$user_id}
                AND abm.parent_id IS NULL
                GROUP BY abt.id
                ORDER BY abt.create_date DESC
                LIMIT ".$limit."
                OFFSET ".$offset
            ;
            $query = $this->db->query($sql);
            return $query->result_array();
        }

        /**
         * Insert new data the table aruaru_bbs_messages
         * @param unknown $data: this is the value that inserted in the table
         */
        function insert($data) 
        {
            $data['create_date'] = date('Y-m-d H:i:s');
            $data['board_id'] = BOARD_ID;
            $this->db->insert('aruaru_bbs_messages', $data);
            return  $this->db->insert_id();
        }

        /**
         * comment Overlap duplication additional prevention
         */
        function commentOverlapCheck($data) 
        {
            $this->db->select('id, message');
            $this->db->where('board_id', 2);
            $this->db->where('thread_id', $data['thread_id']);
            $this->db->order_by("create_date", "DESC");
            $this->db->limit(1);
            $query = $this->db->get('aruaru_bbs_messages');
            if ($query->num_rows() > 0) {
                $array = $query->result_array();
                if ($array[0]['message'] == $data['message']) {
                    return false;
                }
                return true;
            } else {
                return true;
            }
        }


        /**
         * Serialize all the comments into array by
         * call the functions getCommentByParent
         * @param string $id : id of the latest comments
         * @param number $offset
         * @param number $limit
         * @return multitype: array of comments
         */
        function getLatestsArray($id = null, $offset = 0, $limit = 10) 
        {
            $arr_comments = array();
            $comments = $this->getLatestComments($id, $offset, $limit);

/*            $i = 0;
            foreach ($comments as $comment) {
                $comment['truncated_message'] = HelperApp::truncate_string($comment['message'], 30);

                if ($child = $this->getCommentByParent($comment['id'])) {
                    $comment['reply'][$i] = $child;
                    $i++;
                }
                $arr_comments[$comment['id']] = $comment;
            }*/
            foreach ($comments as $comment) {
                $comment['truncated_message'] = HelperApp::truncate_string($comment['message'], 30);

                if ($child = $this->getCommentByParentAr($comment['id'])) {
                    $comment['reply'] = $child;
                }
                $arr_comments[$comment['id']] = $comment;
            }
            return $arr_comments;
        }

        /**
         * Check if comments by user
         * @param string $user_id
         * @param string $id
         */
        function isUserHasComments($user_id, $id) 
        {
            $this->db->select('thread_id');
            $this->db->where('user_id', $user_id);
            $this->db->where_in('thread_id', $id);
            $this->db->group_by("thread_id"); 
            $query = $this->db->get('aruaru_bbs_messages');
            return $query->result_array();
        }

        /**
         * Updates the likes of the comments
         * @param unknown $id: id of the message
         * @param unknown $react :(like_discount, dislike_count field)
         * @return boolean: check if successfully update 
         */
        function react($id, $react) 
        {
            $this->db->set($react.'_count', '`'.$react.'_count`+1', FALSE);
            $this->db->where('id', $id);
            return $this->db->update('aruaru_bbs_messages');
        }
        
        /**
         * Delete batch message
         * @param string $post_ids
         * @param number $user_id
         */
        function deleteMultipleComments($post_ids, $user_id) 
        {
            $cond = array();
            $this->db->set('publish', 3);
            if(!empty($user_id)){
                $this->db->where('user_id', $user_id);
                $cond['user_id'] = $user_id;
            }
            $this->db->where_in('thread_id', $post_ids);
            $this->db->update('aruaru_bbs_messages');

            # deduct points to commenters target=2
            #deduct all aruaru weekly points for the comments target=3
            $this->load->model('Point_model', 'point');
            $cond['target'] = array('2', '3');
            $cond['thread_id'] = $post_ids;
            $cond['bonus_requested_flag'] = 0;
            $rows = $this->point->getAruaruPoints($post_ids, $user_id, array(2,3));
            $result = $this->point->deduct($cond);
            if ($result) {
                foreach ($rows as $key => $value) {
                    $point_msg[2] = '回答ボーナスを引く';
                    $point_msg[3] = '私もそう思う）を引く（回答者)';
                    $this->point->updateScoutBonus($value['user_id'], $value['point'] , $point_msg[$value['target']], 1);
                }
            }

            #deduct all aruaru points for the comments target=4
            $cond['target'] = 4;
            if(!empty($user_id))
                $cond['comment_id'] = ' IS NOT NULL';
            $rows = $this->point->getAruaruPoints($post_ids, $user_id, array(4));
            $result = $this->point->deduct($cond);
            if ($result) {
                foreach ($rows as $key => $value) {
                 $this->point->updateScoutBonus($value['user_id'], $value['point'] , '私もそう思うを引く（質問者）', 1);
                }
            }
        }

        /**
         * Select message that has reply
         * @param string $id
         * @return boolean
         */
        function hasReply($id = null) 
        {
            if (is_null($id)) return false;
            $this->db->select('id');
            $this->db->from('aruaru_bbs_messages');
            $this->db->where('parent_id', $id);
            $this->db->where('publish', 1);
            $query = $this->db->query($sql, $id);
            if ($query->row_array()) return true;
            else return false;
        }
		
        /**
         * Get total sql calculation rows
         */
        public function get_total_rows() 
        {
            $sql = "SELECT FOUND_ROWS() as total";
            $query = $this->db->query($sql);
            return $query->row_array();  
        }

    }
