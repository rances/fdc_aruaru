<?php
    class Point_model extends CI_Model
    {
        function __construct() {
            parent::__construct();
        }
        
        /**
         * Insert new point in aruaru_points
         * @param unknown $data: inserted value
         */
        function insert($data) {

            $commenter_can_point = $this->user_model->getsPoint($data['user_id']);
            if ($data['target'] == 4) {
                $data['point'] = ($commenter_can_point)? $this->getPointValue($data) : 0;
            } elseif(!$commenter_can_point) {
                return;
            } else {
                $data['point'] = $this->getPointValue($data);
            }

            $data['create_date'] = date('Y-m-d H:i:s');
            $data['create_ip'] = $_SERVER['REMOTE_ADDR'];
            $this->db->insert('aruaru_bbs_points', $data);
            return  $data;
        }


        /**
         * Get point value from aruaru_points
         * @param $target: value is based on target
         */
        function getPointValue($data) {
            switch ($data['target']) {
                case 1:
                    return $this->getPointSettingValue('thread_bonus');
                    break;
                case 2:
                    return $this->getPointSettingValue('comment_bonus');
                    break;
                case 3:
                    $times = $this->getPointSettingValue('like_points_multiply_by');
                    return $data['point'] * $times;
                    break;
                case 4:
                    return $this->getPointSettingValue('comment_like_bonus');
                    break;
                default:
                    return 0;
                    break;
            }

        }

        /**
         * Add weekly aruaru pointt
         */
        function addWeeklyAruaruPoints() {

            $times = $this->getPointSettingValue('like_points_multiply_by');
            $add_date = date('Y-m-d H:i:s');
            $sql = "
                INSERT INTO aruaru_bbs_points(user_id,thread_id,point,target,create_date)
                SELECT 
                    abt.user_id, 
                    abp.thread_id,
                    SUM(abp.point) * ".$times." as point,
                    3 as target,
                    '".$add_date."' as create_date
                FROM
                    aruaru_bbs_points AS abp
                        LEFT JOIN
                    aruaru_bbs_threads AS abt ON abp.thread_id = abt.id
                WHERE
                    abp.validity = 1 AND abp.target = 4
                        AND abt.bonus_requested_flag = 0
                        AND abt.publish = 1
                        AND abp.create_date > DATE_SUB(CURDATE(), INTERVAL + 1 WEEK)
                        AND abp.create_date < CURDATE()
                GROUP BY abp.thread_id
            ";
            $this->db->query($sql);
            $added_point =  $this->db->affected_rows();
            $this->addWeelklyScoutBonus($add_date);
            return $added_point;
        }

        /**
         * Get weekly aruaru pointt
         */
        function checkWeeklyPointAdded() {
            $sql = "
                SELECT 
                abp.user_id
                FROM aruaru_bbs_points AS abp 
                WHERE abp.validity = 1 
                AND abp.target = 3 
                AND abp.create_date > DATE_SUB(CURDATE(), INTERVAL + 1 WEEK)
            ";

            $query = $this->db->query($sql);
            return ($query->num_rows() > 0)? true : false;
        }


        /**
         * Get point value from aruaru_bbs_setting table
         * @param $name: column name
         */
        function getPointSettingValue($name) {
            if(empty($name)) return;
            $sql = 'SELECT value FROM aruaru_bbs_settings WHERE name = ?';
            $query = $this->db->query($sql, $name);
            $data =  $query->row_array();
            return $data['value'];
        }

        /**
         * Get point settings from aruaru_bbs_setting table
         */
        function getPointSettings() {
            $data = array();
            $sql = 'SELECT * FROM aruaru_bbs_settings';
            $query = $this->db->query($sql);

            foreach($query->result_array() as $setting ){
                $data[$setting['name']] = $setting['value'];
            }
            return $data;
        }


        /**
         * Invalidate points
         * @param unknown $conditions: array
         */
        function deduct($conditions) {
            $this->db->set('validity', 0);
            foreach ($conditions as $name => $value) {
                if(is_array($value))
                    $this->db->where_in($name, $value);
                else if($name == 'comment_id')
                    $this->db->where($name.$value);
                else
                    $this->db->where($name, $value);
            }

            return $this->db->update('aruaru_bbs_points');
        }

        /**
         * @author: VJS
         * @name : updateScoutBonus
         * @todo : add a new record for bonus data
         * @param  user id
         * @return TRUE: success, FALSE: failed
         */
        public function updateScoutBonus($user_id = null, $addPoint = null, $reason = '', $mode = 0){
           
            if($addPoint <= 0) return false;

            if ($user_id  == null) return;
            // get Total points
            if ($mode == 1) {
                $cur = $this->getRemainPointslog($user_id);
                $new_bonus_money = $cur['new_bonus_money'];

                $old_bonus_money = $new_bonus_money;
                $new_bonus_money = $new_bonus_money - $addPoint;
                $addPoint = '-'.$addPoint;
            } else {
                $new_bonus_money = HelperApp::curlTotalPoints($user_id);
                if(!$new_bonus_money) {
                    $row = $this->getUserAllSiteMoney($user_id);
                    $new_bonus_money = $row['bonus_money'];
                }
                $old_bonus_money = $new_bonus_money - $addPoint;
            }

            $sql  = "INSERT INTO  aruaru_bbs_points_log SET ";
            $sql .= "user_id = ?, bonus_money = ?, old_bonus_money = ?, ";
            $sql .= "new_bonus_money = ?, reason = ?, created_date = NOW()";
            $params = array($user_id, $addPoint, $old_bonus_money, $new_bonus_money, $reason);
            $this->db->query( $sql, $params );
        }

        public function getUserAllSiteMoney($userId, $valid = 1) {
          $sql = "
                SELECT 
                    IFNULL(SUM(money), 0) AS bonus_money
                FROM
                    (SELECT 
                        bonus_requested_flag, SUM(IFNULL(point, 0)) AS money
                    FROM
                        aruaru_bbs_points
                    WHERE
                        user_id = $userId
                        AND validity = $valid
                        AND bonus_requested_flag = 0
                    UNION SELECT 
                        bonus_requested_flag, IFNULL(bonus_money, 0) AS money
                    FROM
                        scout_mail_bonus
                    WHERE
                        user_id = $userId
                        AND display_flag = $valid
                    UNION SELECT 
                        bonus_requested_flag, SUM(IFNULL(point, 0)) AS money
                    FROM
                        bbs_points
                    WHERE
                        user_id = $userId 
                        AND validity = $valid
                        AND bonus_requested_flag = 0
                        ) AS t
                WHERE
                    t.bonus_requested_flag = 0
              ";
          $query = $this->db->query($sql);
          return $query->row_array();
        }

        public function getRemainPointslog($id = null) {
           $sql = "
                SELECT 
                    bonus_money, old_bonus_money, new_bonus_money
                FROM
                    aruaru_bbs_points_log
                WHERE
                    user_id = $id
                ORDER BY id DESC
                LIMIT 1    
            ";

            $query = $this->db->query($sql, $id);
            return $query->row_array();
        }

        public function getAruaruPoints($thread_id = null, $user_id = null, $target = null) {
            $this->db->where_in('thread_id', $thread_id);
            if(!empty($user_id)) {
                $this->db->where('user_id', $user_id);
                if(in_array(4, $target))
                    $this->db->where('comment_id IS NOT NULL');
            }
            $this->db->where_in('target', $target);
            $this->db->where('bonus_requested_flag', 0);            
            $this->db->where('validity', 1);
            $this->db->from('aruaru_bbs_points');
            $query =  $this->db->get();
            return $query->result_array();
        }

        public function addWeelklyScoutBonus($add_date) {
            $sql = "
                SELECT
                    user_id,
                    thread_id,
                    point
                FROM
                    aruaru_bbs_points
                WHERE 
                   create_date = ? 
            ";
            $query = $this->db->query($sql, $add_date);
            $result = $query->result_array();
            foreach ($result as $point) {
               $this->updateScoutBonus($point['user_id'], $point['point'] , '私もそう思う（回答者）');
            }
        }
               
    }
