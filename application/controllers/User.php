<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('post_model');
        $this->load->model('comment_model');
        $this->load->model('bonus_model');

    }

	public function my_account() {
        $this->requireLogin();
        $this->viewData["title"] = 'マイページ';
        $post = $this->input->post();
        if(empty($post['pagination'])){
            $mypage = array(
                    'big_id' =>  'my_page',
                    'm_jpname' =>'マイページ'
                    );
            $this->breadcrumbs->staticUrL($mypage);

            $latestPost = $this->post_model->getUsersLatestPosts($this->user['id']);
            $latestComment = $this->post_model->getUsersLatestPostsByComments($this->user['id']);

            $this->viewData['latest_post'] = $latestPost;
            $this->viewData['latest_comment'] = $latestComment;
            $this->viewData['tab_1_title'] = (!empty($latestPost)) ? '投稿したお題' : 'お題を投稿する';
            #show points for mobile and machemoba and maquia users
            if($this->agent->is_mobile() && $this->user['user_can_point']){
                $this->load->model('bonus_model');
                $this->load->model('point_model');
                $this->viewData["bonusData"] = $this->bonus_model->getUserBonusData($this->user['id']);
                $this->viewData["pointSettings"] = $this->point_model->getPointSettings();
            }

            $this->twig->display('mypage.html', $this->viewData);

        }else {

            if(!empty($post['pagination'])){

                $content = (empty($post['content']))? '':$post['content'];
                
                // get post count
                $count = 0;
                if($content == 'comments'){
                    $count  = $this->post_model->getUsersLatestPostsByCommentCount($this->user['id']);
                }else if($content == 'posts'){
                    $count  = $this->post_model->getUsersLatestPostCount($this->user['id']);
                    $this->viewData['tab_1_title'] = (count($count) > 0) ? '投稿したお題' : 'お題を投稿する';
                }

                $paged = $this->paginate($post,$count);

                if($paged){
                    if( $paged['paginate']== 'load'){
                        $this->viewData['pagination'] = $paged;
                        $this->twig->display('pagination.html', $this->viewData);  
                    }else{
                        if($content  == 'comments'){
                            $posts = $this->post_model->getUsersLatestPostsByComments($this->user['id'], $paged['start']);
                        }elseif($content == 'posts'){
                            $posts = $this->post_model->getUsersLatestPosts($this->user['id'], $paged['start']);
                        }
                        $this->viewData['content'] = $content;
                        $this->viewData['moreposts'] = $posts;
                        $this->twig->display('mypage_more.html', $this->viewData);
                    }     
                    return ;
                }
            }
                
        }

    }

    public function deletePosts() {
        $this->requireLogin();
        $post = $this->input->post();
        $verified_ids = array();

        if(!empty($post['values']) && !empty($post['content'])){

            # check if user really owns the posts
            if($post['content'] == 'posts'){
                $verified_ids = $this->post_model->isUsersPost($this->user['id'],$post['values']); 
                $verified_ids = array_unique(array_column($verified_ids,'id'));
            }
            # check if user really owns the comments
            else  if($post['content'] == 'comments'){
                $verified_ids = $this->comment_model->isUserHasComments($this->user['id'],$post['values']);
                $verified_ids = array_unique(array_column($verified_ids,'thread_id'));
            }

            if($post['content'] == 'posts') 
                $this->post_model->deleteMultiplePosts($verified_ids, $this->user['id']);
            else if($post['content'] == 'comments') 
                $this->comment_model->deleteMultipleComments($verified_ids, $this->user['id']);
            
        }

        echo json_encode(array('ids' => $verified_ids));
        return;
    }
    

    
    /**
    * @name: aruaru_logout
    * @desc: unsets user id session
    * @parameters: none
    * @return: ajax response
    */
    public function aruaru_logout() {
        $return = array();
        HelperApp::clear_session();
        if(!isset($_SESSION['id'])) {
            $return['status'] = 'success';
        }
        echo json_encode($return);
        exit;
    }

    public function my404() {
        $this->twig->display('404.html');
    }

    
}

?>