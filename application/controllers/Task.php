<?php
class Task extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if(!$this->input->is_cli_request()) show_error('Direct access is not allowed');
    }

    function addWeeklyPoint()
    {
        $this->load->model('point_model');

    	
    	$givenPoint = $this->point_model->checkWeeklyPointAdded();
    	if($givenPoint) {
    		echo 'This week\' points already given.';
    		return;
    	}
        
        $user_points = $this->point_model->addWeeklyAruaruPoints();
    	if(empty($user_points))
    		echo 'No Points given this week.';
    	else
    		echo date('Y-m-d H:i:s').': aruaru_bbs_points table updated '.$user_points.' rows.';
        return;
    }


    function updateCommentAruaruRank()
    {
        $this->load->model('commentRank_model');
        if($result = $this->commentRank_model->updateRanking()){
            echo date('Y-m-d H:i:s').': aruaru_bbs_messages_rank table updated '.$result.' rows.';
        }else{
            echo date('Y-m-d H:i:s').': failed to update aruaru_bbs_messages_rank table.';
        }

    }


}
