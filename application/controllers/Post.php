<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends MY_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('bulletin_model');
        $this->load->model('category_model');
        $this->load->model('post_model');
        $this->load->model('comment_model');
        $this->load->model('point_model');

		$this->load->model('thread_model');

    }

	public function index($id)
	{
		$post = $this->post_model->getPostDetail($id);
		if (empty($post['id']) || !$post['publish']) {
			header("HTTP/1.0 404 Not Found");
	        $this->twig->display('404.html', $this->viewData);
	        return;
        }


        //if(empty($post['id']) || !$post['publish']) redirect(base_url(), 'refresh');



/*
        $big_cate_info = $this->category_model->getBigCateInfo($info['big_cate_id']);
       	$this->viewData['advert_info'] = $big_cate_info;*/
		//var_dump($big_cate_info);


        $request = $this->input->post();
		$is_ajax = (empty($request['is_ajax'])) ? false : true;

		if(!$is_ajax){

			$this->breadcrumbs
			 ->getCategory()
             ->setJoinThread()
             ->setWhere('bb_thread.id', $id)
             ->get();
		}


        if ($post['time_ago'] > 1440) {
            $post['create_date_sp'] = date('Y年m月d日H時i分',strtotime($post['create_date']));
        } else {
            $post['create_date_sp'] = $post['time_ago'];            
        }

        $this->viewData['owner'] = array(
        	'id' => $post['owner_id'], 
        	'nick_name' => $post['owner_nickname']
        );

        if(!empty($this->user['id']) && $post['owner_id'] == $this->user['id'])
        	$this->viewData['is_owner'] = true;

        $this->viewData['post'] = $post;

		$offset = (!empty($request['offset'])) ? $request['offset'] : 0;
        $comments = $this->comment_model->getLatestsArray($id, $offset);

        $i = ($offset > 0)? $offset : 0;
        foreach ($comments as $key => $val) {
        	$comments[$key]['index'] = $i;
        	$i++;
        }
		$this->viewData['comments'] = $comments;


		/* 広告枠 */

		$info = $this->thread_model->getThreadinfoAdvert($id);
//		$this->viewData['th_info'] = $info;
//        $big_cate_info = $this->category_model->getBigCateInfo($info['big_cate_id']);
       	$b_info = $this->category_model->getBigCateInfo($info['big_cate_id']);
		$b_info['ad_image'] = ltrim($b_info['ad_image'], '/');
       	$this->viewData['advert_info'] =$b_info;

        $top_info = $this->category_model->getIsTopInfo();
        $top_info['ad_image'] = ltrim($top_info['ad_image'], '/');
        $this->viewData['top_advert_info'] =$top_info;

//		$array = array("damy", "ああああああああ", "いいいいいいい", "ううううううう");
		$this->viewData['advertise'] = array('url' => 'https://app.adjust.com/8cil0f' , 'img' => '/sp/image/banner/sample_3.png');
//		$this->viewData['advertise'] = array('url' => 'https://joyspe.com/' , 'img' => '/sp/image/banner/sample_2.gif');

        $get_url = urlencode(FULL_URL);
        $this->viewData['twitter_share_url'] = $get_url;
        $this->viewData['twitter_share_text'] =  HelperApp::truncate_string($post['message'], 30);
        
		$temp = ($is_ajax) ? 'more_comments':'reaction';

		$this->load->model('viewlogs_model');

		# log user access for post popularity
		if(!$is_ajax){
			$log_data['thread_id'] = $post['id'];
			$log_data['big_cate_id'] =  $post['big_cate_id'];
			$log_data['cate_id'] =  $post['cate_id'];		
			$this->logAccess($log_data);
		}

		# popular post based on Category from the last 3 days
		$log_data = array(
            'row' => 'avbl.cate_id',
            'value' => $post['cate_id'] 
        );
        $popular = $this->viewlogs_model->get3daysLogs($log_data);
        $this->viewData['popular'] = $popular;

        # initialize meta array in reaction
        $this->meta_array[0][$id] = array( 
        	'title' => $post['big_cate_name'],
        	'description' => '',
        	);
 		$this->get_meta_tags($id, 0); # call for meta value
 		$this->viewData['title'] = $post['title'].' | ジョイスペ';
 		$this->viewData['meta_desc'] = "【".$post['big_cate_name']."あるある】".$post['message'];
 		$this->viewData['colorSwitch'] = $this->point_model->getPointSettingValue('aruaru_like_tally_switch');
        $this->twig->display($temp.'.html', $this->viewData);
	}

	public function ajaxImagefileUpload(){
		$userfile = $_FILES['userfile']['name'];
		$upload_image_file = $userfile;
		list($file_name,$file_type) = explode(".",$upload_image_file);
		//ファイル名を日付と時刻にしている。
		$name = date("YmdHis").".".$file_type;

		$config['file_name'] = $name;
		$config['upload_path'] = 'uploads/temp/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '3000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload()) {
//			echo "error";
		} else {
//			echo "upload_success".$userfile;
		}

        $data_ar['filename'] = $name;
        $data_ar['flag'] = true;
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($data_ar));
	}

	public function ajaxCommentConfirm()
	{

        $text = $this->input->post('comment');
        $pid = $this->input->post('pid');

        $userfile = $this->input->post('image_file_name');

		$url_ar = array();

		if(preg_match_all('(https?://[-_.!~*\'()a-zA-Z0-9;/?:@&=+$,%#]+)', $text, $result) !== false){
		    foreach ($result[0] as $value){
		        //URL表示
				$url_ar[] = $value;
		    }
		}
        mb_language('Japanese');

		// スクレイピング
		$this->load->library('simple_html_dom');
		$parser = new Simple_html_dom();
		$page_info_ar = array();

		foreach ($url_ar as $key => $value) {
	        $url = $value;
	        $html = file_get_contents($url);
            $html = mb_convert_encoding($html, 'utf8', 'auto');
	       	$parser->load($html);

	        $title = $parser->find('title', 0)->plaintext;
	        if($parser->find("meta[name='description']", 0) != null) {
				$meta_description = $parser->find("meta[name='description']", 0)->content;
	        } else {
				$meta_description = '';
	        }

	 		$page_info_ar[$key]['title'] = $title;
	 		$page_info_ar[$key]['description'] = $meta_description;
	 		$page_info_ar[$key]['source_url'] = $url;

	 		$image_ar = array();

            $html_type = (preg_match('/<!doctype html>/i', $html, $matches) == 1)? 'html5':'html';
            $img_find = ($html_type == 'html5')? 'article img':'img';

	        foreach($parser->find($img_find) as $element){
				if(preg_match('/(https:\/\/|http:\/\/)(.*?)\//', $element->src, $matches) != 1){
					if(preg_match('/^\/(.*)/', $element->src, $matches) == 1){
						$url_temp_ar = parse_url($url);
						$scheme = $url_temp_ar['scheme'];
						$host = $url_temp_ar['host'];
						$url_tmp = $scheme.'://'.$host.'/';
						$src = preg_replace('/^\//', '', $element->src);
					} else {
						$src = preg_replace('/^\.\//', '', $element->src);
					}
		            $image_ar[] = $url_tmp.$src;
				} else {
		            $image_ar[] = $element->src;
				}
	        }
			$page_info_ar[$key]['image_ar'] = $image_ar;
		}

		$str = preg_replace('(https?://[-_.!~*\'()a-zA-Z0-9;/?:@&=+$,%#]+)', '/--URLIMAGE--/', $text);
		$ar = preg_match_all('/\/--URLIMAGE--\//', $str, $m, PREG_OFFSET_CAPTURE);

		$i = 0;
		foreach ($url_ar as $key => $val) {
			$str = substr_replace($str, sprintf("/--IMAGE%03d--/",$i), $m[0][$i][1], strlen('/--URLIMAGE--/'));
			$i++;
		}
		$message_all = $str;
		$preview_str = nl2br($str);

		$i = 0;
		foreach ($url_ar as $key => $value) {
			$this->viewData['info'] = $page_info_ar[$i];
			$this->viewData['info_num'] = $i;
			$template = $this->twig->render('reaction_url_template.html', $this->viewData);
			$pattern = '/--IMAGE'.sprintf("%03d",$i).'--/';
			$str = str_replace($pattern, $template, $str);
			$i++;
		}

		$i = 0;
		foreach ($url_ar as $key => $value) {
			$this->viewData['info'] = $page_info_ar[$i];
			$this->viewData['info_num'] = $i;
			$template = $this->twig->render('reaction_url_template.html', $this->viewData);
			$pattern = '/--IMAGE'.sprintf("%03d",$i).'--/';
			$preview_str = str_replace($pattern, $template, $preview_str);
			$i++;
		}


		$this->viewData['message'] = $str;
		$this->viewData['message_all'] = $message_all;
		$this->viewData['page_id'] = $pid;

        $data_ar = array();
        $data_ar['page_id'] = $pid;
        $data_ar['message_all'] =  $message_all;
        $data_ar['message'] = $str;
        $data_ar['preview'] = $preview_str;
        if ($userfile != '') {
			$upload_image_path = base_url().'uploads/temp/'. $userfile;
			$data_ar['upload_image_path'] = $upload_image_path;
			$data_ar['upload_image_file'] = $userfile;
        } else {
			$data_ar['upload_image_path'] = false;
        }
        $data_ar['flag'] = true;

        //$dataをJSONにして返す
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($data_ar));
	}

	public function add_conf($text)
	{
		$url_ar = array();

		if(preg_match_all('(https?://[-_.!~*\'()a-zA-Z0-9;/?:@&=+$,%#]+)', $text, $result) !== false){
		    foreach ($result[0] as $value){
		        //URL表示
				$url_ar[] = $value;
		    }
		}
        mb_language('Japanese');

		// スクレイピング
		$this->load->library('simple_html_dom');
		$parser = new Simple_html_dom();
		$page_info_ar = array();

		foreach ($url_ar as $key => $value) {
	        $url = $value;
	        $html = file_get_contents($url);
            $html = mb_convert_encoding($html, 'utf8', 'auto');
	       	$parser->load($html);

	        $title = $parser->find('title', 0)->plaintext;

	        if($parser->find("meta[name='description']", 0) != null) {
				$meta_description = $parser->find("meta[name='description']", 0)->content;
	        } else {
				$meta_description = '';
	        }

	 		$page_info_ar[$key]['title'] = $title;
	 		$page_info_ar[$key]['description'] = $meta_description;
	 		$page_info_ar[$key]['source_url'] = $url;

	 		$image_ar = array();

            $html_type = (preg_match('/<!doctype html>/i', $html, $matches) == 1)? 'html5':'html';
            $img_find = ($html_type == 'html5')? 'article img':'img';

	        foreach($parser->find($img_find) as $element){
				if(preg_match('/(https:\/\/|http:\/\/)(.*?)\//', $element->src, $matches) != 1){
					$url_temp_ar = parse_url($url);
					$scheme = $url_temp_ar['scheme'];
					$host = $url_temp_ar['host'];
					$url_tmp = $scheme.'://'.$host.'/';
					if(preg_match('/^\/(.*)/', $element->src, $matches) == 1){
						$src = preg_replace('/^\//', '', $element->src);
					} else {
						$src = preg_replace('/^\.\//', '', $element->src);
					}
		            $image_ar[] = $url_tmp.'/'.$src;
				} else {
		            $url = $element->src;
		            $image_ar[] = $url;
				}
	        }
			$page_info_ar[$key]['image_ar'] = $image_ar;
		}
		$str = preg_replace('(https?://[-_.!~*\'()a-zA-Z0-9;/?:@&=+$,%#]+)', '/--URLIMAGE--/', $text);
		$ar = preg_match_all('/\/--URLIMAGE--\//', $str, $m, PREG_OFFSET_CAPTURE);

		$i = 0;
		foreach ($url_ar as $key => $val) {
			$str = substr_replace($str, sprintf("/--IMAGE%03d--/",$i), $m[0][$i][1], strlen('/--URLIMAGE--/'));
			$i++;
		}
		$message_all = $str;
		$preview_str = nl2br($str);

		$i = 0;
		foreach ($url_ar as $key => $value) {
			$this->viewData['info'] = $page_info_ar[$i];
			$this->viewData['info_num'] = $i;
			$template = $this->twig->render('theme_url_template.html', $this->viewData);
			$pattern = '/--IMAGE'.sprintf("%03d",$i).'--/';
			$str = str_replace($pattern, $template, $str);
			$i++;
		}

		$i = 0;
		foreach ($url_ar as $key => $value) {
			$this->viewData['info'] = $page_info_ar[$i];
			$this->viewData['info_num'] = $i;
			$template = $this->twig->render('theme_url_template.html', $this->viewData);
			$pattern = '/--IMAGE'.sprintf("%03d",$i).'--/';
			$preview_str = str_replace($pattern, $template, $preview_str);
			$i++;
		}

		$bl_max = count($url_ar);

        $data_ar = array();
        $data_ar['message_all'] =  $message_all;
        $data_ar['message'] = $str;
        $data_ar['preview'] = $preview_str;
        $data_ar['bl_max'] = $bl_max;

        return $data_ar;
	}

	public function add_message_gen($pid)
	{
		$data = array();
		$up_image_name_ar = array();
		if ($bl_max > 0) {
			$bl = $this->input->post('bl');
			$bl_description = $this->input->post('bl_description');
			$bl_title = $this->input->post('bl_title');
			$bl_source_url = $this->input->post('bl_source_url');

			$str = $this->input->post('message_all');

			$upload_path = "uploads/images/".$pid;
			//ディレクトリを作成してその中にアップロード
			if(!file_exists($upload_path)){
				mkdir($upload_path,0777);
			}

			for ($i=0; $i < $bl_max; $i++) {
				$this->viewData['info_title'] = $bl_title[$i];
				$this->viewData['info_description'] = $bl_description[$i];

				/* ここから */
				$bl_image = $bl[$i];
				$file = preg_replace('/(.*)\//', '', $bl_image);
				list($file_name,$file_type) = explode(".",$file);
				$name = date("YmdHis").$i.".".preg_replace('/\?(.*)/', '', $file_type);
				$up_image_name_ar[$i] = $name;
				$this->viewData['info_image'] = '/'.$upload_path.'/'.$name;
				/* ここまで */

				$this->viewData['info_source_url'] = $bl_source_url[$i];
				$template = $this->twig->render('reaction_url_put_template.html', $this->viewData);
				$pattern = '/--IMAGE'.sprintf("%03d",$i).'--/';
				$str = str_replace($pattern, $template, $str);

			}
			$data['message'] = $str;
			$tw_comment = $str;
		} else {
			$data['message'] = $post['comment'];
			$tw_comment = $post['comment'];
		}
	}


	public function add()
	{
		$this->requireLogin();
		$this->load->library(array('form_validation'));
		$bulletins = array();
		$post = $this->input->post();
		$page = 'page_theme';

		$post_suf = array('','あるある','にありがちなこと','でありがちなこと','にしかわからないこと','');

		if($post){
			$this->form_validation->set_rules('big_cate_id','大カテゴリー', 'trim|required', array('required' => '大カテゴリーを選択してください。'));
			$this->form_validation->set_rules('cate_id','小カテゴリー', 'trim|required', array('required' => '小カテゴリーを選択してください。'));
			$this->form_validation->set_rules('qcl','', 'trim|required', array('required' => 'どれかひとつ選択してください。'));
			$this->form_validation->set_rules('title','お題', 'trim|required|min_length[2]|max_length[30]', array(
					'required' => 'お題を入力してください。',
					'min_length' => '全角2文字以上で入力してください。',
					'max_length' => '全角30文字以内で入力してください。'
					));
			$this->form_validation->set_rules('message','本文', 'trim|required|min_length[5]|max_length[500]', array(
					'required' => '本文を入力してください。',
					'min_length' => '全角5文字以上で入力してください。',
					'max_length' => '全角500文字以内で入力してください。'
					));
			if($this->form_validation->run()){
				$post['title'] = trim($post['title']);
				$post['qclval']  = $post_suf[intval($post['qcl'])];

				if(!empty($post['confirm'])){
					$page = 'theme_conf';
					$category = $this->category_model->getRowById($post['cate_id']);
					$bulletin = $this->bulletin_model->getRowById($post['big_cate_id']);
					$post['cate_name'] = $category['jp_name'];
					$post['big_cate_name'] = $bulletin['jp_name'];
					$post['user_nickname'] =  $this->user['nick_name'];
					$post['image_file_name'] =  $post['image_file_name'];

					$message_next = $message_prev = $post['message'];
					$post['message_prev'] = $message_prev;

					$message_ar = $this->add_conf($message_next);
					$post['message_next'] = $message_ar['preview'];

					$post['bl_max'] = $message_ar['bl_max'];

        			$post['message_all'] =  $message_ar['message_all'];

				}else if(!empty($post['save'])){
					$page = 'theme_comp';

					//access IP
					$ip = $this->input->ip_address();

					$message = $post['go_message'];
					$message_all = $post['message_all'];
					$bl_max = $post['bl_max'];

					$data = array(
						'big_cate_id' => $post['big_cate_id'],
						'cate_id' => $post['cate_id'],
						'title' => $post['title'].' '.$post['qclval'],
						'user_id' => $this->user['id'],
						'create_ip' => $ip
					);

					$pid = $this->post_model->insert($data);
					if(empty($pid)) 
						redirect(base_url(), 'refresh');

					# add point to owner
					$point = array(
						'user_id' => $this->user['id'], 
						'thread_id' => $pid,
						'target' => 1
					);
					$point_id = $this->addPoint($point);
					if ($point_id)
						$this->point_model->updateScoutBonus($this->user['id'], $point_id['point'], '投稿ボーナス');
					$this->viewData['pid'] =  $pid;

					/* urlから取得生成 */
					$data = array();
					$up_image_name_ar = array();
					if ($bl_max > 0) {
						$bl = $post['bl'];
						$bl_description = $post['bl_description'];
						$bl_title = $post['bl_title'];
						$bl_source_url = $post['bl_source_url'];

						$str2 = $str = $post['message_all'];

						//ディレクトリを作成してその中にアップロード
						$upload_path = "uploads/images/".$pid;
						if(!file_exists($upload_path)){
							mkdir($upload_path,0777);
						}

						for ($i=0; $i < $bl_max; $i++) {
							$this->viewData['info_title'] = $bl_title[$i];
							$this->viewData['info_description'] = $bl_description[$i];

							/* ここから */
							$bl_image = $bl[$i];
							if ($bl_image != '') {
								$file = $bl_image;
								$file = preg_replace('/(.+?)\?.*/', '\\1', $file);
								$file_type = $this->get_img_extension($file);
								$name = date("YmdHis").$i.".".$file_type;
								$up_image_name_ar[$i] = $name;
								$this->viewData['info_image'] = '/'.$upload_path.'/'.$name;
							} else {
								$up_image_name_ar[$i] = '';
								$this->viewData['info_image'] = '';
							}
							/* ここまで */

							$this->viewData['info_source_url'] = $bl_source_url[$i];
							$template = $this->twig->render('theme_url_put_template.html', $this->viewData);
							$pattern = '/--IMAGE'.sprintf("%03d",$i).'--/';
							$str = str_replace($pattern, $template, $str);

							$template = $this->twig->render('theme_url_twput_template.html', $this->viewData);
							$str2 = str_replace($pattern, $template, $str2);
						}
						$data['message'] = $str;

						/*$str2 = str_replace(array("\r\n", "\r", "\n"), '', $str2);
						$pattern = '/\/\-\-IMAGE[0-9][0-9][0-9]\-\-\//';
						$str2 = preg_replace($pattern, '', $str2);*/
						$tw_comment = $str2;
					} else {
						$data['message'] = $post['message_all'];
						$tw_comment = $post['message_all'];
					}

					/* 画像が投稿されたら true */
					$title_cat = false;
					$latest_up_load_file = null;

					$upload_image_file = $this->input->post('userfile');
					if ($upload_image_file != ''){
						list($file_name,$file_type) = explode(".",$upload_image_file);
						//ファイル名を日付と時刻にしている。
						$name = date("YmdHis").".".$file_type;
						$file = "uploads/images/".$pid;
						//ディレクトリを作成してその中にアップロードしている。
						if(!file_exists($file)){
							mkdir($file,0777);
						}
						$move_path = $file.'/'.$name;
						rename("uploads/temp/".$upload_image_file, $move_path);
						$data['up_image'] = '/'.$move_path;
						$latest_up_load_file = $move_path;

						$title_cat = true;
					}

					/* 画像ダウンロード */
					if ($bl_max > 0) {
						$bl = $post['bl'];
						$upload_path = "uploads/images/".$pid;
						for ($i=0; $i < $bl_max; $i++) {
							$bl_image = $bl[$i];
							if ($bl_image != '') {
							    $contents_data = file_get_contents($bl_image);
							    file_put_contents($upload_path.'/'.$up_image_name_ar[$i], $contents_data);
							}
						}
						if (!$title_cat) {
							if ($up_image_name_ar[0] != '') {
								$latest_up_load_file = $upload_path.'/'.$up_image_name_ar[0];
								$title_cat = true;
							} else {
								$latest_up_load_file = null;
							}
						}
					}

					//twitterPOST
					$this->load->model('thread_model');
					$thread_info = $this->thread_model->getThreadinfoTw($pid);

					$bulletin = $this->bulletin_model->getRowById($post['big_cate_id']);

					$media_ids = 0;
					if ($title_cat) {
						$data['title'] = '【画像】'.$post['title'].' '.$post['qclval'];
						$data['image_up_flag'] = 1;
						$title = '【画像】'.'【'.$bulletin['jp_name'].'あるある】'.$post['title'].' '.$post['qclval'];
					} else {
						$title = '【'.$bulletin['jp_name'].'あるある】'. $post['title'].' '.$post['qclval'];
					}
					$this->post_model->updateData($pid, $data);

//					$tw_text = $title.' '.base_url().'reaction/'.$pid.' '.mb_strimwidth($tw_comment, 0,130,"...");
					$tw_text = $title.' '.base_url().'reaction/'.$pid.' '.$tw_comment;
					$tw_text = mb_strimwidth($tw_text, 0,130, '...');
					//$this->postTwitter($tw_text);
				}
			}else{
				$this->viewData['error'] = $this->form_validation->error_array();
			}
		}
		$static = array(
            'page_theme' => 'お題を作る',
            'theme_conf' => '入力内容の確認',
            'theme_comp' => 'お題の投稿完了'
        );
		$this->breadcrumbs->staticUrL(array(
                'big_id' =>  'page_theme',
                'm_jpname' => $static[$page]
                ));
		$bulletins = $this->bulletin_model->getRecords('id,jp_name');
        $this->viewData['bulletins'] = $bulletins;
        $this->viewData['values'] = $post;
        $this->viewData['nick_name'] = $this->user['nick_name'];
		$this->twig->display($page.'.html', $this->viewData);

		/* お題の投稿完了時処理 */
		if ($page == 'theme_comp') {
			if ($latest_up_load_file == null) {
				$this->post_tw($tw_text,'', $pid);
			} else {
				$this->post_tw($tw_text, $latest_up_load_file, $pid);
			}
		}

	}

	public function post_tw($text, $image_path = '', $pid = 0)
	{

        $http_host = $_SERVER['HTTP_HOST'];
        if ($http_host == 'aruaru.joyspe.com') {
			$twitter = array(
			  'consumer_key' => 'ypQ15ofHDkPRqHuyhedXfmaqb',
			  'consumer_secret' => '58iKxocq7wgMQAAFR94QyMpDJofGFfClfuJDFbJrQo6FJFVO5K',
			  'token' => '715392288269045763-4NGx0ADsOu2UMK6dos7rZDlrE1Jg0s4',
			  'secret' => 'CslD9IFhqqEuqyLCS9Cx4ydHel1JCY5D5dDrXv1lBmQMg',
			  'curl_ssl_verifypeer' => false ,
			);
        } else {
	        /* 旧アカウント */
			$twitter = array(
			  'consumer_key' => 'a5a6Sv0beCatkJIUNluqf6lcA',
			  'consumer_secret' => 'hdONP8kAvSI1TQTpgEVbJqNDtvGkSh3skcchpUDTWp4gsbErys',
			  'token' => '715391831731638272-mzxzIEF1eg35KPfhdUoWo7PdrIHgkF1',
			  'secret' => 'DUkX1mgsXMVpYc978vjeXMLkA2EiLluFP44HfY9CvGCt8',
			  'curl_ssl_verifypeer' => false ,
			);
        }

		$this->load->library('TmhOAuth', $twitter);

		if($image_path != '' ) {

			$http_response_header = array();

	      //画像設定
			$path = $image_path; //画像のパス指定
//	      $path = './uploads/tweet_image/'.'aruaru_tweet1.png'; //画像のパス指定
			//$data = file_get_contents($path); //画像データ取得

			$error_message = '';
			//file_get_contents関数でデータを取得
			if($data = @file_get_contents($path)){
			    //ここにデータ取得が成功した時の処理
				$image = base64_encode($data); //base64でエンコード

				$images = array(
				  'media_data' => $image //画像データを指定
				);

				try {
					$image_upload = $this->tmhoauth->request('POST', 'https://upload.twitter.com/1.1/media/upload.json', $images, true, true);
				}catch (Exception $e){
					//echo "Error occured!! This Script was terminated.";
					mb_send_mail("suzuki_kiyo@innetwork.jp", 'あるあるTW画像リクエストエラー','画像リクエストエラーpid='.$pid, "From:suzuki_kiyo@innetwork.jp");
					exit;
				}

				//画像id格納
				$decode = json_decode($this->tmhoauth->response["response"], true); //JSONデコードして
				$media_id = $decode['media_id_string']; //「media_id_string」の値を格納

				$message = array(
				  'media_ids' => $media_id, //格納した画像idを指定
				  'status' => $text
				);

				try {
				  $this->tmhoauth->request('POST', $this->tmhoauth->url('1.1/statuses/update'), $message, true, false);
				}catch (Exception $e){
					//echo "Error occured!! This Script was terminated.";
					mb_send_mail("suzuki_kiyo@innetwork.jp", 'あるあるツイートリクエストエラー画像あり','ツイートリクエストエラー'.$pid, "From:suzuki_kiyo@innetwork.jp");
				  exit;
				}

			}else{
			    //エラー処理
			    if(count($http_response_header) > 0){
			        //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
			        $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

			        //エラーの判別
			        switch($status_code[1]){
			            //404エラーの場合
			            case 404: $error_message = "指定したページが見つかりませんでした";
			                break;
			 
			            //500エラーの場合
			            case 500: $error_message = "指定したページがあるサーバーにエラーがあります";
			                break;
			 
			            //その他のエラーの場合
			            default: $error_message = "何らかのエラーによって指定したページのデータを取得できませんでした";
			        }
			    }else{
			        //タイムアウトの場合 or 存在しないドメインだった場合
			        $error_message = "タイムエラー or URLが間違っています";
			    }

				mb_send_mail("suzuki_kiyo@innetwork.jp", 'あるある画像取得エラー', $error_message.'('.$pid.')', "From:suzuki_kiyo@innetwork.jp");
			}

		} else {

	      $message = array(
	          'status' => $text
	      );

	      try {
	          $this->tmhoauth->request('POST', $this->tmhoauth->url('1.1/statuses/update'), $message, true, false);
	      }catch (Exception $e){
				mb_send_mail("suzuki_kiyo@innetwork.jp", 'あるあるツイートリクエストエラーテキストのみ','ツイートリクエストエラー'.$pid, "From:suzuki_kiyo@innetwork.jp");
	          	//echo "Error occured!! This Script was terminated.";
	          	exit;
	      }

		}
	}



	public function post_tw_back($text, $image = '')
	{
		if($image == '' ) {
	        $result = file_get_contents(
	            base_url().'twitter_text_api',
	          false,
	          stream_context_create(
	            array(
	              'http' => array(
	                'method' => 'POST',
	                'header' => implode(
	                  "\r\n",
	                  array(
	                    'Content-Type: application/x-www-form-urlencoded'
	                  )
	                ),
	                'content' => http_build_query(
	                  array(
	                    'text' => $text,
	                    'media_id' => null
	                  )
	                )
	              )
	            )
	          )
	        );
		} else {
	        $result = file_get_contents(
	            base_url().'twitter_img_api',
	          false,
	          stream_context_create(
	            array(
	              'http' => array(
	                'method' => 'POST',
	                'header' => implode(
	                  "\r\n",
	                  array(
	                    'Content-Type: application/x-www-form-urlencoded'
	                  )
	                ),
	                'content' => http_build_query(
	                  array(
	                    'text' => $text,
	                    'image' => $image
	                  )
	                )
	              )
	            )
	          )
	        );
		}
	}

	public function get_img_extension($path)
	{
		//getimagesize関数で画像情報を取得する $path:画像ファイルのパスやURL
		list($img_width, $img_height, $mime_type, $attr) = getimagesize($path);
		//list関数の第3引数にはgetimagesize関数で取得した画像のMIMEタイプが格納されているので条件分岐で拡張子を決定する
		switch($mime_type){
		    //jpegの場合
		    case IMAGETYPE_JPEG:
		        //拡張子の設定
		        $img_extension = "jpg";
		        break;
		    //pngの場合
		    case IMAGETYPE_PNG:
		    //拡張子の設定
		        $img_extension = "png";
		        break;
		    //gifの場合
		    case IMAGETYPE_GIF:
		        //拡張子の設定
		        $img_extension = "gif";
		        break;
		}
		//拡張子の出力
		return $img_extension;
	}

	public function ajaxLatest()
	{
		$post = $this->input->post();
		if(empty($post['offset']) AND empty($post['limit'])) return;
//		$latestPost = $this->post_model->getLatestPosts($post['offset'], $post['limit']);
		$latestPost = $this->post_model->getLatestPostsV2($post['offset'], $post['limit']);
        $big_category_ar = array();
        $ar = $this->bulletin_model->get_big_category_ar();
        foreach ($ar as $key => $val) {
            $big_category_ar[$val['id']] = $val;
        }
        foreach ($latestPost as $key => $val) {
            $big_cate_id = $val['big_cate_id'];
            $latestPost[$key]['big_cate_slug'] =  mb_strtolower(str_replace(" ", "_", $big_category_ar[$big_cate_id]['name']));
            $latestPost[$key]['big_cate_name'] = $big_category_ar[$big_cate_id]['jp_name'];
        }
		$this->viewData['latest_post'] = $latestPost;
		$this->twig->display('more_latest_post.html', $this->viewData);
	}

	public function ajaxAddComment()
	{
		$post = $this->input->post();
		$comment = (!empty($post['comment']))? trim($post['comment']):'';
		if(empty($comment)){
			echo "Error:コメントを入力してください。"; return;
		}else if(!empty($post['pid'])){
    		$post_i = $this->post_model->getPostCommentCount($post['pid']);

    		if($post_i){
    			$user_id = (empty($this->user['id']))? 0 : $this->user['id'];
    			$data['user_id'] = $user_id;

				$bl_max = 0;
				$bl_max = $this->input->post('bl_max');
				$up_image_name_ar = array();
				if ($bl_max > 0) {
					$bl = $this->input->post('bl');
					$bl_description = $this->input->post('bl_description');
					$bl_title = $this->input->post('bl_title');
					$bl_source_url = $this->input->post('bl_source_url');

					$str2 = $str = $this->input->post('message_all');

					$upload_path = "uploads/images/".$post['pid'];
					//ディレクトリを作成してその中にアップロードしている。
					if(!file_exists($upload_path)){
						mkdir($upload_path,0777);
					}

					for ($i=0; $i < $bl_max; $i++) {
						$this->viewData['info_title'] = $bl_title[$i];
						$this->viewData['info_description'] = $bl_description[$i];

						/* ここから */
						$bl_image = $bl[$i];
						if($bl_image != '') {
							$file = $bl_image;
							$file = preg_replace('/(.+?)\?.*/', '\\1', $file);
							//list($file_name,$file_type) = explode(".",$file);
							$file_type = $this->get_img_extension($file);
							$name = date("YmdHis").$i.".".$file_type;
							//$name = date("YmdHis").$i.".".preg_replace('/\?(.*)/', '', $file_type);
							$up_image_name_ar[$i] = $name;
							$this->viewData['info_image'] = '/'.$upload_path.'/'.$name;
						} else {
							$up_image_name_ar[$i] = '';
							$this->viewData['info_image'] = '';
						}
						/* ここまで */

						$this->viewData['info_source_url'] = $bl_source_url[$i];
						$template = $this->twig->render('reaction_url_put_template.html', $this->viewData);
						$pattern = '/--IMAGE'.sprintf("%03d",$i).'--/';
						$str = str_replace($pattern, $template, $str);

						/* 重複検査用データ */
						$this->viewData['info_image'] = $bl[$i];
						$template = $this->twig->render('reaction_url_put_template.html', $this->viewData);
						$pattern = '/--IMAGE'.sprintf("%03d",$i).'--/';
						$str2 = str_replace($pattern, $template, $str2);
					}
	    			$data['message'] = $str;
	    			$tw_comment = $str;
				} else {
	    			$data['message'] = $post['comment'];
	    			$tw_comment = $post['comment'];
				}

				/* 画像が投稿されたら true */
				$title_cat = false;
				$latest_up_load_file = null;

				$upload_image_file = $this->input->post('userfile');
				if ($upload_image_file != ''){
					list($file_name,$file_type) = explode(".",$upload_image_file);
					//ファイル名を日付と時刻にしている。
					$name = date("YmdHis").".".$file_type;
					$file = "uploads/images/".$post['pid'];
					//ディレクトリを作成してその中にアップロードしている。
					if(!file_exists($file)){
						mkdir($file,0777);
					}
					$move_path = $file.'/'.$name;
					rename("uploads/temp/".$upload_image_file, $move_path);
					$data['up_image'] = '/'.$move_path;
					$latest_up_load_file = $move_path;
					$title_cat = true;
				}

    			$data['thread_id'] = $post['pid'];

    			if(!empty($post['cid'])){
    				$data['parent_id'] = $post['cid'];
    				$this->viewData['is_reply'] = true;
    			}else{
    				$this->viewData['is_comment'] = true;
    			}

				if ($bl_max > 0) {
	    			$data['message'] = $str2;
					if (!$this->comment_model->commentOverlapCheck($data)) {
						return;
					}
	    			$data['message'] = $str;
				} else {
					if (!$this->comment_model->commentOverlapCheck($data)) {
						return;
					}
				}

				$ip = $this->input->ip_address();
				$data['create_ip'] = $ip;

	   			$new_id = $this->comment_model->insert($data);
    			$new_comment = $this->comment_model->getCommentDetail($new_id);


				/* コメント時間、あるある数更新 */
				$latestPost = $this->post_model->getLatestPosts_comment_time_by_threads_id(0,10,$post['pid']);
				$this->post_model->getLatestPosts_comment_time_update($latestPost);

				/* 画像ダウンロード */
				if ($bl_max > 0) {
					$bl = $this->input->post('bl');
					$upload_path = "uploads/images/".$post['pid'];
					for ($i=0; $i < $bl_max; $i++) {
						$bl_image = $bl[$i];
						if ($bl_image != '') {
						    $data = file_get_contents($bl_image);
						    file_put_contents($upload_path.'/'.$up_image_name_ar[$i], $data);
						}
					}
					$latest_up_load_file = $upload_path.'/'.$up_image_name_ar[0];
					$title_cat = true;
				}

				//twitterPOST
				$this->load->model('thread_model');
				$thread_info = $this->thread_model->getThreadinfoTw($post['pid']);

				if ($title_cat) {
					$data = array();
					if ($thread_info['image_up_flag'] == 0) {
						$data['title'] = '【画像】'.$thread_info['title'];
						$data['image_up_flag'] = 1;
						$this->post_model->updateData($post['pid'], $data);
						$message = preg_replace('/<("[^"]*"|\'[^\']*\'|[^\'">])*>/','',$thread_info['message']);
						$bulletin = $this->bulletin_model->getRowById($thread_info['big_cate_id']);
						$tw_text = '【画像】【'.$bulletin['jp_name'].'あるある】'.$thread_info['title'].' '.base_url().'reaction/'.$post['pid'].' '.$message;
						$tw_text = mb_strimwidth($tw_text, 0,130, '...');
						$this->post_tw($tw_text, $latest_up_load_file);
					}
				}

    			if($new_comment['id']){

    				# add point to owner 
    				if(empty($post['cid'])){
    					$this->load->model('point_model');
    					$max_comment_bonus = $this->point_model->getPointSettingValue('max_comment_has_bonus');
    					if($post_i['comment_count'] < $max_comment_bonus){
    						$point = array(
								'user_id' => $user_id,
								'thread_id' => $post['pid'],
								'target' => 2
							);
    						$point_id = $this->addPoint($point);
    						if ($point_id)
    							$this->point_model->updateScoutBonus($this->user['id'], $point_id['point'], '回答ボーナス');						
    					}
    				}
			 		$this->viewData['colorSwitch'] = $this->point_model->getPointSettingValue('aruaru_like_tally_switch');

			 		$new_comment['truncated_message'] = HelperApp::truncate_string($new_comment['message'], 30);

                    $url = base_url().'reaction/'.$post['pid'];
                    $this->viewData['twitter_share_url'] = urlencode($url);
    				$this->viewData['comment'] = $new_comment;
    				$this->viewData['post'] = $post_i;
    				$this->twig->display('add_comment.html', $this->viewData);
    				return;
    			}
    		}    			
        }
        echo "Error:エラーが発生しました。"; return;
	}

	function ajaxReact()
	{
		$res = array();
		$cid = null;
		$post = $this->input->post();
		if(empty($post['react']) AND empty($post['pid'])) return;

		$react = array('yes'=>'like','no'=>'dislike');
		
		$post_i = $this->post_model->getPost($post['pid']);
		if(empty($post_i['id']) || empty($react[$post['react']])) return;

		$point = array( 
			'thread_id' => $post['pid'],
			'target' => 4
		);
		$rank = 0;
		$this->load->model('user_model');

		if (isset($post['cid'])) $cid = $post['cid'];

		$check_like = $this->post_model->check_ip($post_i['user_id'], $post['pid'], $cid, $_SERVER['REMOTE_ADDR']);
		if (!$check_like) return;			

		if(empty($post['cid'])) {
			if($this->post_model->react($post['pid'], $react[$post['react']])){
	
				$res['success'] = true;
				$point['user_id'] = $post_i['user_id'];

				/* コメント時間、あるある数更新 */
				$latestPost = $this->post_model->getLatestPosts_comment_time_by_threads_id(0,10,$post['pid']);
				$this->post_model->getLatestPosts_comment_time_update($latestPost);
			}
			$point_msg = '私もそう思う';
		}else{
			$comment = $this->comment_model->getComment($post['cid']);
			if($this->comment_model->react($post['cid'], $react[$post['react']])){
				
				$res['success'] = true;
				$point['user_id'] = $comment['user_id'];
				$point['comment_id'] = $post['cid'];

				/* コメント時間、あるある数更新 */
				$latestPost = $this->post_model->getLatestPosts_comment_time_by_threads_id(0,10,$post['pid']);
				$this->post_model->getLatestPosts_comment_time_update($latestPost);
			}
			$rank = $comment['rank'];
			$point_msg = '私もそう思う（質問者）';
		}

		$res['option_number'] = $this->getOption($rank);

		# add point to comment owner 
		if($post['react'] == 'yes'){
			$point_id = $this->addPoint($point);
			if($point_id)
				$this->point_model->updateScoutBonus($point['user_id'], $point_id['point'], $point_msg);
		}

		echo json_encode($res);
		return;
	}

	function ajaxPosts_comment_time_update()
	{
		/* コメント時間、あるある数更新 */
		$latestPost = $this->post_model->getLatestPosts_comment_time();
		$this->post_model->getLatestPosts_comment_time_update($latestPost);
		return;
	}
    
    public function ajaxDeletePosts() {
        $res = array();
        $post = $this->input->post();
        $post_ids = $post['checked_posts'];
        if (!empty($post_ids)) {
            $postids_in_str = implode(", ", $post_ids);
            $this->post_model->deleteMultiplePosts($postids_in_str, $post['user_id']);
            $res['success'] = true;
        } else {
            return;
        } 
        echo json_encode($res);
		return;        
    }
    
    public function ajaxDeletePostsComment() {
        $res = array();
        $post = $this->input->post();
        $post_ids = $post['checked_posts'];
        if (!empty($post_ids)) {
            $postids_in_str = implode(", ", $post_ids);
            $this->comment_model->deleteMultipleComments($postids_in_str, $post['user_id']);
            $res['success'] = true;
        } else {
            return;
        } 
        echo json_encode($res);
		return;        
    }

    function logAccess($data){
    	if($this->viewlogs_model->alreadyAccessToday($data['thread_id']))
		$this->viewlogs_model->insert($data);
    }

    function addPoint($point){
    	$this->load->model('point_model');
    	$this->load->model('user_model');
		return $this->point_model->insert($point);
    }

    function getOption($rank = 0) {    	
	 	$colorSwitch = $this->point_model->getPointSettingValue('aruaru_like_tally_switch');
	 	$option = 0;
 		if ($colorSwitch == 1) {
 			if($rank > 80) $option = 1;
 			else if($rank > 50) $option = 2;
 			else if($rank > 5) $option = 3;
 			else $option = 4;
		}
		return $option;
    }

	/* production ONLY */
    function postTwitter($tw_text, $media_id = 0) {
        $http_host = $_SERVER['HTTP_HOST'];
        if ($http_host != 'aruaru.joyspe.com') {
            return true;
        }
        // 設定
/*
        $api_key = 'a5a6Sv0beCatkJIUNluqf6lcA' ;        // APIキー
        $api_secret = 'hdONP8kAvSI1TQTpgEVbJqNDtvGkSh3skcchpUDTWp4gsbErys' ;        // APIシークレット
        $access_token = '715391831731638272-mzxzIEF1eg35KPfhdUoWo7PdrIHgkF1' ;      // アクセストークン
        $access_token_secret = 'DUkX1mgsXMVpYc978vjeXMLkA2EiLluFP44HfY9CvGCt8' ;        // アクセストークンシークレット[_TWITTER_OAUTH_1_]
*/

        $api_key = 'ypQ15ofHDkPRqHuyhedXfmaqb' ;        // APIキー
        $api_secret = '58iKxocq7wgMQAAFR94QyMpDJofGFfClfuJDFbJrQo6FJFVO5K' ;        // APIシークレット
        $access_token = '715392288269045763-4NGx0ADsOu2UMK6dos7rZDlrE1Jg0s4' ;      // アクセストークン
        $access_token_secret = 'CslD9IFhqqEuqyLCS9Cx4ydHel1JCY5D5dDrXv1lBmQMg' ;        // アクセストークンシークレット[_TWITTER_OAUTH_1_]
        $request_url = 'https://api.twitter.com/1.1/statuses/update.json' ;     // エンドポイント
        $request_method = 'POST' ;
//        $media_ids = 743685123363606528;
        // パラメータA (リクエストのオプション)

        if ($media_id == 0) {
	        $params_a = array(
	            'status' => $tw_text,
	//          'status' => 'APIを通して投稿なう。 投稿時間: ' . date( 'Y/m/d H:i' ) . '投稿元: https://joyspe.com/' ,
	        ) ;
        } else {
	        $params_a = array(
	            'status' => $tw_text,
	//          'status' => 'APIを通して投稿なう。 投稿時間: ' . date( 'Y/m/d H:i' ) . '投稿元: https://joyspe.com/' ,
	            'media_ids' => $media_id,
	        ) ;
        }

        // キーを作成する (URLエンコードする)
        $signature_key = rawurlencode( $api_secret ) . '&' . rawurlencode( $access_token_secret ) ;

        // パラメータB (署名の材料用)
        $params_b = array(
            'oauth_token' => $access_token ,
            'oauth_consumer_key' => $api_key ,
            'oauth_signature_method' => 'HMAC-SHA1' ,
            'oauth_timestamp' => time() ,
            'oauth_nonce' => microtime() ,
            'oauth_version' => '1.0' ,
        ) ;

        // パラメータAとパラメータBを合成してパラメータCを作る
        $params_c = array_merge( $params_a , $params_b ) ;

        // 連想配列をアルファベット順に並び替える
        ksort( $params_c ) ;

        // パラメータの連想配列を[キー=値&キー=値...]の文字列に変換する
        $request_params = http_build_query( $params_c , '' , '&' ) ;

        // 一部の文字列をフォロー
        $request_params = str_replace( array( '+' , '%7E' ) , array( '%20' , '~' ) , $request_params ) ;

        // 変換した文字列をURLエンコードする
        $request_params = rawurlencode( $request_params ) ;

        // リクエストメソッドをURLエンコードする
        // ここでは、URL末尾の[?]以下は付けないこと
        $encoded_request_method = rawurlencode( $request_method ) ;
     
        // リクエストURLをURLエンコードする
        $encoded_request_url = rawurlencode( $request_url ) ;
     
        // リクエストメソッド、リクエストURL、パラメータを[&]で繋ぐ
        $signature_data = $encoded_request_method . '&' . $encoded_request_url . '&' . $request_params ;

        // キー[$signature_key]とデータ[$signature_data]を利用して、HMAC-SHA1方式のハッシュ値に変換する
        $hash = hash_hmac( 'sha1' , $signature_data , $signature_key , TRUE ) ;

        // base64エンコードして、署名[$signature]が完成する
        $signature = base64_encode( $hash ) ;

        // パラメータの連想配列、[$params]に、作成した署名を加える
        $params_c['oauth_signature'] = $signature ;

        // パラメータの連想配列を[キー=値,キー=値,...]の文字列に変換する
        $header_params = http_build_query( $params_c , '' , ',' ) ;

        // リクエスト用のコンテキスト
        $context = array(
            'http' => array(
                'method' => $request_method , // リクエストメソッド
                'header' => array(            // ヘッダー
                    'Authorization: OAuth ' . $header_params ,
                ) ,
            ) ,
        ) ;

        // オプションがある場合、コンテキストにPOSTフィールドを作成する
        if( $params_a )
        {
            $context['http']['content'] = http_build_query( $params_a ) ;
        }

        // cURLを使ってリクエスト
        $curl = curl_init() ;
        curl_setopt( $curl , CURLOPT_URL , $request_url ) ;
        curl_setopt( $curl , CURLOPT_HEADER, 1 ) ; 
        curl_setopt( $curl , CURLOPT_CUSTOMREQUEST , $context['http']['method'] ) ;         // メソッド
        curl_setopt( $curl , CURLOPT_SSL_VERIFYPEER , false ) ;                             // 証明書の検証を行わない
        curl_setopt( $curl , CURLOPT_RETURNTRANSFER , true ) ;                              // curl_execの結果を文字列で返す
        curl_setopt( $curl , CURLOPT_HTTPHEADER , $context['http']['header'] ) ;            // ヘッダー
        if( isset( $context['http']['content'] ) && !empty( $context['http']['content'] ) )
        {
            curl_setopt( $curl , CURLOPT_POSTFIELDS , $context['http']['content'] ) ;           // リクエストボディ
        }
        curl_setopt( $curl , CURLOPT_TIMEOUT , 5 ) ;                                        // タイムアウトの秒数
        $res1 = curl_exec( $curl ) ;
        $res2 = curl_getinfo( $curl ) ;
        curl_close( $curl ) ;

        // 取得したデータ
        $json = substr( $res1, $res2['header_size'] ) ;             // 取得したデータ(JSONなど)
        $header = substr( $res1, 0, $res2['header_size'] ) ;        // レスポンスヘッダー (検証に利用したい場合にどうぞ)

        // [cURL]ではなく、[file_get_contents()]を使うには下記の通りです…
        // $json = @file_get_contents( $request_url , false , stream_context_create( $context ) ) ;

        // JSONをオブジェクトに変換
        $obj = json_decode( $json ) ;

        // エラー判定
        if( !$json || !$obj )
        {
            //error;
            return false;
        }
        return true;
    }

    public function postTwitterImage($file_path = '')
    {
            $http_host = $_SERVER['HTTP_HOST'];
            if ($http_host != 'aruaru.joyspe.com') {
                return true;
            }
            $doc_root = $_SERVER['DOCUMENT_ROOT'];
    /**************************************************
        メディアアップロード
        // 下記のエンドポイントに対応
        https://api.twitter.com/1.1/account/update_profile_background_image.json    (背景画像)
        https://api.twitter.com/1.1/account/update_profile_image.json   (アイコン画像)
        https://api.twitter.com/1.1/account/update_profile_banner.json  (バナー画像)
        https://upload.twitter.com/1.1/media/upload.json    (画像、動画のメディアアップロード)
    **************************************************/

        // 設定
/*
        $api_key = 'a5a6Sv0beCatkJIUNluqf6lcA' ;        // APIキー
        $api_secret = 'hdONP8kAvSI1TQTpgEVbJqNDtvGkSh3skcchpUDTWp4gsbErys' ;        // APIシークレット
        $access_token = '715391831731638272-mzxzIEF1eg35KPfhdUoWo7PdrIHgkF1' ;      // アクセストークン
        $access_token_secret = 'DUkX1mgsXMVpYc978vjeXMLkA2EiLluFP44HfY9CvGCt8' ;        // アクセストークンシークレット[_TWITTER_OAUTH_2_]
*/
        $api_key = 'ypQ15ofHDkPRqHuyhedXfmaqb' ;        // APIキー
        $api_secret = '58iKxocq7wgMQAAFR94QyMpDJofGFfClfuJDFbJrQo6FJFVO5K' ;        // APIシークレット
        $access_token = '715392288269045763-4NGx0ADsOu2UMK6dos7rZDlrE1Jg0s4' ;      // アクセストークン
        $access_token_secret = 'CslD9IFhqqEuqyLCS9Cx4ydHel1JCY5D5dDrXv1lBmQMg' ;        // アクセストークンシークレット[_TWITTER_OAUTH_1_]

        $request_url = 'https://upload.twitter.com/1.1/media/upload.json' ;     // エンドポイント
        $request_method = 'POST' ;

        $file = $doc_root.$file_path;

        // パラメータA (リクエストのオプション)
        $params_a = array(
            'media_data' => base64_encode( @file_get_contents($file) ) ,
        ) ;

        // キーを作成する (URLエンコードする)
        $signature_key = rawurlencode( $api_secret ) . '&' . rawurlencode( $access_token_secret ) ;

        // パラメータB (署名の材料用)
        $params_b = array(
            'oauth_token' => $access_token ,
            'oauth_consumer_key' => $api_key ,
            'oauth_signature_method' => 'HMAC-SHA1' ,
            'oauth_timestamp' => time() ,
            'oauth_nonce' => microtime() ,
            'oauth_version' => '1.0' ,
        ) ;

        // リクエストURLにより、メディアを指定するパラメータが違う
        switch( $request_url )
        {
            case( 'https://api.twitter.com/1.1/account/update_profile_background_image.json' ) :
            case( 'https://api.twitter.com/1.1/account/update_profile_image.json' ) :
                $media_param = 'image' ;
            break ;

            case( 'https://api.twitter.com/1.1/account/update_profile_banner.json' ) :
                $media_param = 'banner' ;
            break ;

            case( 'https://upload.twitter.com/1.1/media/upload.json' ) :
                $media_param = ( isset($params_a['media']) && !empty($params_a['media']) ) ? 'media' : 'media_data' ;
            break ;
        }

        // イメージデータの取得
        $media_data = ( isset( $params_a[ $media_param ] ) ) ? $params_a[ $media_param ] : '' ;

        // 署名の材料から、動画データを除外する
        if( isset( $params_a[ $media_param ] ) ) unset( $params_a[ $media_param ] ) ;

        // バウンダリーの定義
        $boundary = '---------------' . md5( mt_rand() ) ;

        // POSTフィールドの作成 (まずはメディアのパラメータ)
        $request_body = '' ;
        $request_body .= '--' . $boundary . "\r\n" ;
        $request_body .= 'Content-Disposition: form-data; name="' . $media_param . '"; ' ;
    //  $request_body .= 'filename="' . time() . '"' ;  // [filename]は指定不要なので、おサボり…
        $request_body .= "\r\n" ;
    //  [Content-Type]と[Content-Transfer-Encoding]は指定不要なので、おサボり…
    //  $mimetype = 〜   //Mime/Typeを調べる処理
    //  $request_body .= "Content-Type: " . $mimetype . "\r\n" ;
    //  $request_body .= "Content-Transfer-Encoding: \r\n" ;
        $request_body .= "\r\n" . $media_data . "\r\n" ;

        // POSTフィールドの作成 (その他のオプションパラメータ)
        foreach( $params_a as $key => $value )
        {
            $request_body .= '--' . $boundary . "\r\n" ;
            $request_body .= 'Content-Disposition: form-data; name="' . $key . '"' . "\r\n\r\n" ;
            $request_body .= $value . "\r\n" ;
        }

        // リクエストボディの作成
        $request_body .= '--' . $boundary . '--' . "\r\n\r\n" ;

        // リクエストヘッダーの作成
        $request_header = "Content-Type: multipart/form-data; boundary=" . $boundary ;

        // パラメータAとパラメータBを合成してパラメータCを作る → ×
    //  $params_c = array_merge( $params_a , $params_b ) ;
        $params_c = $params_b ;     // 署名の材料にオプションパラメータを加えないこと

        // 連想配列をアルファベット順に並び替える
        ksort( $params_c ) ;

        // パラメータの連想配列を[キー=値&キー=値...]の文字列に変換する
        $request_params = http_build_query( $params_c , '' , '&' ) ;

        // 一部の文字列をフォロー
        $request_params = str_replace( array( '+' , '%7E' ) , array( '%20' , '~' ) , $request_params ) ;

        // 変換した文字列をURLエンコードする
        $request_params = rawurlencode( $request_params ) ;

        // リクエストメソッドをURLエンコードする
        // ここでは、URL末尾の[?]以下は付けないこと
        $encoded_request_method = rawurlencode( $request_method ) ;
     
        // リクエストURLをURLエンコードする
        $encoded_request_url = rawurlencode( $request_url ) ;
     
        // リクエストメソッド、リクエストURL、パラメータを[&]で繋ぐ
        $signature_data = $encoded_request_method . '&' . $encoded_request_url . '&' . $request_params ;

        // キー[$signature_key]とデータ[$signature_data]を利用して、HMAC-SHA1方式のハッシュ値に変換する
        $hash = hash_hmac( 'sha1' , $signature_data , $signature_key , TRUE ) ;

        // base64エンコードして、署名[$signature]が完成する
        $signature = base64_encode( $hash ) ;

        // パラメータの連想配列、[$params]に、作成した署名を加える
        $params_c['oauth_signature'] = $signature ;

        // パラメータの連想配列を[キー=値,キー=値,...]の文字列に変換する
        $header_params = http_build_query( $params_c , '' , ',' ) ;

        // リクエスト用のコンテキスト
        $context = array(
            'http' => array(
                'method' => $request_method , // リクエストメソッド
                'header' => array(            // ヘッダー
                    'Authorization: OAuth ' . $header_params ,
                    'Content-Type: multipart/form-data; boundary= ' . $boundary ,
                ) ,
                'content' => $request_body ,
            ) ,
        ) ;

        // cURLを使ってリクエスト
        $curl = curl_init() ;
        curl_setopt( $curl , CURLOPT_URL , $request_url ) ;
        curl_setopt( $curl , CURLOPT_HEADER, 1 ) ; 
        curl_setopt( $curl , CURLOPT_CUSTOMREQUEST , $context['http']['method'] ) ;         // メソッド
        curl_setopt( $curl , CURLOPT_SSL_VERIFYPEER , false ) ;                             // 証明書の検証を行わない
        curl_setopt( $curl , CURLOPT_RETURNTRANSFER , true ) ;                              // curl_execの結果を文字列で返す
        curl_setopt( $curl , CURLOPT_HTTPHEADER , $context['http']['header'] ) ;            // ヘッダー
        curl_setopt( $curl , CURLOPT_POSTFIELDS , $context['http']['content'] ) ;           // リクエストボディ
        curl_setopt( $curl , CURLOPT_TIMEOUT , 5 ) ;                                        // タイムアウトの秒数
        $res1 = curl_exec( $curl ) ;
        $res2 = curl_getinfo( $curl ) ;
        curl_close( $curl ) ;

        // 取得したデータ
        $json = substr( $res1, $res2['header_size'] ) ;             // 取得したデータ(JSONなど)
        $header = substr( $res1, 0, $res2['header_size'] ) ;        // レスポンスヘッダー (検証に利用したい場合にどうぞ)

        // [cURL]ではなく、[file_get_contents()]を使うには下記の通りです…
        // $json = @file_get_contents( $request_url , false , stream_context_create( $context ) ) ;

        // JSONをオブジェクトに変換
        $obj = json_decode( $json ) ;


        // エラー判定
        if ( !$json || !$obj ) {
            echo "データを更新することができませんでした…。設定を見直して下さい。";
        }
        return $obj->{'media_id_string'};
    }

}
