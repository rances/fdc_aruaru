<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('post_model');
        $this->load->model('comment_model');
        $this->load->model('viewlogs_model');
        $this->load->model('category_model');
    }

	public function index()
	{
        
        $latestPost = $this->post_model->getLatestPostsV2();
        $big_category_ar = array();
        $ar = $this->bulletin_model->get_big_category_ar();
        foreach ($ar as $key => $val) {
            $big_category_ar[$val['id']] = $val;
        }
        foreach ($latestPost as $key => $val) {
            $big_cate_id = $val['big_cate_id'];
            $latestPost[$key]['big_cate_slug'] =  mb_strtolower(str_replace(" ", "_", $big_category_ar[$big_cate_id]['name']));
            $latestPost[$key]['big_cate_name'] = $big_category_ar[$big_cate_id]['jp_name'];
        }

        $latestComment = $this->post_model->getLatestPostsByComments();
        $this->viewData['latest_post'] = $latestPost;
        $this->viewData['latest_comment'] = $latestComment;


        if ($this->agent->is_mobile()) {
            $popular = $this->viewlogs_model->get3daysLogsHome('',0,5);
        } else {
            $popular = $this->viewlogs_model->get3daysLogs();
        }

//        $popular = $this->viewlogs_model->get3daysLogs();
        $this->viewData['popular'] = $popular;

        $top_info = $this->category_model->getIsTopInfo();
        $top_info['ad_image'] = ltrim($top_info['ad_image'], '/');
        $this->viewData['top_advert_info'] =$top_info;

        $this->twig->display('index.html', $this->viewData);
	}
}
