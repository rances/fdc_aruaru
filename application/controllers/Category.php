<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

    public $page_thread_limit = 10;

    public function __construct() {
        parent::__construct();

        $this->load->model('category_model');
        $this->load->model('post_model');
        $this->load->model('viewlogs_model');
    }

	public function index($cat_id)
    {
        $category = $this->category_model->getCateDetail($cat_id);
        if ($category['id'] == null) {
            header("HTTP/1.0 404 Not Found");
            $this->twig->display('404.html');
            return;
        }

        $this->get_meta_tags($cat_id, 2); // call for meta value
        if ($this->agent->is_mobile()) {
            $this->breadcrumbs->isParent = false;
        }
        $this->breadcrumbs
             ->getCategory()
             ->setWhere('sub_cat.id', $cat_id)
             ->get();
        //$category = $this->category_model->getCateDetail($cat_id);
        //if(empty($category['id'])) redirect(base_url(), 'refresh');

        $latestComment = $this->post_model->getLatestPostsByCatComment($cat_id);
        $latestPost = $this->post_model->getLatestPostsByCat($cat_id);

        $this->viewData['cat'] = $category;
        $this->viewData['latest_comment'] = $latestComment;
        $this->viewData['latest_post'] = $latestPost;

        $log_data = array(
            'row' => 'avbl.cate_id',
            'value' => $cat_id 
        );

        if ($this->agent->is_mobile()) {
            $popular = $this->viewlogs_model->get3daysLogsV3($log_data,0,5);
        } else {
            $popular = $this->viewlogs_model->get3daysLogs($log_data);
        }
//        $popular = $this->viewlogs_model->get3daysLogs($log_data);
        $this->viewData['popular'] = $popular;

        if($this->agent->is_mobile()){

            $post = $this->input->post();
            if(!empty($post['pagination'])){

                $content = (empty($post['content']))? '':$post['content'];
                
                // get post count
                $count = 0;
                if($content == 'comments'){
                    $count  = $this->post_model->getCatPostByCommentCount($cat_id);
                }else if($content == 'posts'){
                    $count  = $this->post_model->getCatPostCount($cat_id);
                }else if($content == 'popular'){
                    $count = 1;
                }

                $paged = $this->paginate($post,$count);

                if($paged){
                    if( $paged['paginate']== 'load'){
                        $this->viewData['pagination'] = $paged;
                        $this->twig->display('pagination.html', $this->viewData);  
                    }else{
                        if($content  == 'comments'){
                            $posts = $this->post_model->getLatestPostsByCatComment($cat_id, $paged['start']);
                        }elseif($content == 'posts'){
                            $posts = $this->post_model->getLatestPostsByCat($cat_id, $paged['start']);
                        }elseif($content == 'popular'){
                            $posts = $this->viewlogs_model->get3daysLogsV3($log_data,0,5);
                        }
                        $this->viewData['content'] = $content;
                        $this->viewData['posts'] = $posts;
                        $this->twig->display('category_more.html', $this->viewData);
                    }     
                    return ;
                }
            }
                
        }

        $top_info = $this->category_model->getIsTopInfo();
        $top_info['ad_image'] = ltrim($top_info['ad_image'], '/');
        $this->viewData['top_advert_info'] =$top_info;

        $this->twig->display('category.html', $this->viewData);   
	}

    public function ajaxPosts()
    {
        $post = $this->input->post();
        if(empty($post['offset']) AND empty($post['limit']) AND empty($post['id'])) return;


        $latestPost = $this->post_model->getLatestPostsByCat($post['id'], $post['offset'], $post['limit']);
        $this->viewData['latest_post'] = $latestPost;



        $this->twig->display('more_category_post.html', $this->viewData);
    }

    public function menuList()
    {

        $post = $this->input->post();
        if(empty($post['bid'])) return;
        
        $bid = $post['bid'];

        $categorys = $this->category_model->getRecordsByBulletin($bid, 'id,jp_name');
        echo json_encode($categorys);
        return;
    }

}
