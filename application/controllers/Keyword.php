<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keyword extends MY_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('post_model');
        $this->load->model('comment_model');
        $this->load->model('viewlogs_model');
        $this->load->model('keyword_model');
    }

	public function index()
	{
//echo "キーワード検索";
        if ($this->input->get('text')) {

            $keyword = $this->input->get('text');

            $search = str_replace("　", " ", $keyword);
            $this->viewData['search_word'] = $search;

            //キーワードを空白で分割する
            $array = explode(" ", $search);
            $ar = $this->keyword_model->get_keyword_search($array);

            foreach ($ar as $key => $val) {
                $message = $val['message'];
                $message = preg_replace('/<("[^"]*"|\'[^\']*\'|[^\'">])*>/','', $message);
                $ar[$key]['message'] = mb_strimwidth($message, 0,44, '...');
            }
            $this->viewData['keyword_comment'] = $ar;
        }

        $this->twig->template_dir_set(TEMPLATEPATH_SP);

        $this->twig->display('keyword.html', $this->viewData);
	}
}
