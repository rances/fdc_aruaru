<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bulletin extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('category_model');
        $this->load->model('bulletin_model');
        $this->load->model('post_model');
        $this->load->model('viewlogs_model');
    }

    public function index($bid) {
        $categorys = $this->category_model->getCatesByBulletin($bid);

        if(empty($categorys[0]['big_category_id'])) {
            header("HTTP/1.0 404 Not Found");
            $this->twig->display('404.html');
            return;
        }
        $bulletin = $categorys[0];

        $this->get_meta_tags($bid, 1); // call for meta value
        $this->breadcrumbs
             ->setWhere('big_cat.id',$bid)
             ->get();
        if(empty($bid)) redirect(base_url(), 'refresh');

        //$categorys = $this->category_model->getCatesByBulletin($bid);
        //if(empty($categorys[0]['big_category_id'])) redirect(base_url(), 'refresh');
        //$bulletin = $categorys[0];

        $latestComment = $this->post_model->getLatestPostsByBulletinComment($bid);
        $latestPost = $this->post_model->getLatestPostsByBulletinV2($bid);

        foreach ($latestPost as $key => $val) {
            $latestPost[$key]['big_cate_slug'] = $categorys[0]['big_cate_slug'];
            $latestPost[$key]['big_cate_name'] = $categorys[0]['big_cate_jp_name'];
        }

        $this->viewData['categorys'] = $categorys;
        $this->viewData['bulletin'] = $bulletin;
        $this->viewData['latest_comment'] = $latestComment;
        $this->viewData['latest_post'] = $latestPost;

        $log_data = array(
            'row' => 'avbl.big_cate_id',
            'value' => $bulletin['big_category_id'] 
        );


        if ($this->agent->is_mobile()) {
            $popular = $this->viewlogs_model->get3daysLogsV3($log_data,0,5);
        } else {
            $popular = $this->viewlogs_model->get3daysLogs($log_data);
        }
//        $popular = $this->viewlogs_model->get3daysLogs($log_data);
        $this->viewData['popular'] = $popular;


        if($this->agent->is_mobile()){

            $post = $this->input->post();
            if(!empty($post['pagination'])){

                $content = (empty($post['content']))? '':$post['content'];

                // get post count
                $count = 0;
                if($content == 'comments'){
                    $count  = $this->post_model->getPostsByBulletinCommentCount($bid);
                }else if($content == 'posts'){
                    $count  = $this->post_model->getBulletinPostCount($bid);
                }else if($content == 'popular'){
                    $count = 1;
                }

                $paged = $this->paginate($post,$count);

                if($paged){
                    if( $paged['paginate']== 'load'){
                        $this->viewData['pagination'] = $paged;
                        $this->twig->display('pagination.html', $this->viewData);  
                    }else{
                        if($content  == 'comments'){
                            //$moreposts = $this->post_model->getLatestPostsByBulletinComment($bid, $paged['start']);
                            $moreposts = $this->post_model->getLatestPostsByBulletinCommentV2($bid, $paged['start']);
                            foreach ($moreposts as $key => $val) {
                                $moreposts[$key]['big_cate_slug'] = $categorys[0]['big_cate_slug'];
                                $moreposts[$key]['big_cate_name'] = $categorys[0]['big_cate_jp_name'];
                            }
                        }elseif($content == 'posts'){
                            $moreposts = $this->post_model->getLatestPostsByBulletinV2($bid, $paged['start']);
                            foreach ($moreposts as $key => $val) {
                                $moreposts[$key]['big_cate_slug'] = $categorys[0]['big_cate_slug'];
                                $moreposts[$key]['big_cate_name'] = $categorys[0]['big_cate_jp_name'];
                            }
                        }elseif($content == 'popular'){
                            $moreposts = $this->viewlogs_model->get3daysLogsV3($log_data,0,5);
                        }
                        $this->viewData['content'] = $content;
                        $this->viewData['moreposts'] = $moreposts;
                        $this->twig->display('bulletin_more.html', $this->viewData);
                    }     
                    return ;
                }
            }

        }

        $top_info = $this->category_model->getIsTopInfo();
        $top_info['ad_image'] = ltrim($top_info['ad_image'], '/');
        $this->viewData['top_advert_info'] =$top_info;

        $this->twig->display('bulletin.html', $this->viewData);
    }

    public function ajaxPosts()
    {
        $post = $this->input->post();
        if(empty($post['offset']) AND empty($post['limit']) AND empty($post['id'])) return;

        //$latestPost = $this->post_model->getLatestPostsByBulletin($post['id'], $post['offset'], $post['limit']);
        $latestPost = $this->post_model->getLatestPostsByBulletinV2($post['id'], $post['offset'], $post['limit']);
        $categorys = $this->category_model->getCatesByBulletin($post['id']);
        foreach ($latestPost as $key => $val) {
            $latestPost[$key]['big_cate_slug'] = $categorys[0]['big_cate_slug'];
            $latestPost[$key]['big_cate_name'] = $categorys[0]['big_cate_jp_name'];
        }
        $this->viewData['latest_post'] = $latestPost;

        $this->twig->display('more_bulletin_post.html', $this->viewData);
    }
}
