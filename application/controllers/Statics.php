<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statics extends MY_Controller {
    
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
    }

    public function view($html) {
        $static = array(
            'tos' => array(
                'big_id' =>  'tos',
                'm_jpname' =>'利用規約'
                ),
            'company' => array(
                'big_id' =>  'company',
                'm_jpname' =>'会社概要'
                ),
            'privacy' => array(
                'big_id' =>  'privacy',
                'm_jpname' => '個人情報保護方針'
                ),
            'faq' => array(
                'big_id' =>  'faq',
                'm_jpname' => 'FAQ'
                ),
            'contact' => array(
                'big_id' =>  'contact',
                'm_jpname' => 'お問い合わせ'
                ),
            'contact_comp' => array(
                'big_id' =>  'contact_comp',
                'm_jpname' => 'お問い合わせ完了'
                ),
            'guideline' => array(
                'big_id' =>  'guideline',
                'm_jpname' => '禁止事項'
                )            
        );
        $this->breadcrumbs->staticUrL($static[$html]);
        if ($html == 'contact') {
            $this->contact_us();
        }
        $this->twig->display($html.'.html', $this->viewData);
    }
    
    public function contact_us() {
        if($_POST){
           //get values
            $email = trim($this->input->post('contact_email'));
            $subject = trim($this->input->post('contact_subject'));
            $body = trim($this->input->post('contact_message'));
            //set values
            $this->form_validation->set_rules('contact_email', 'メールアドレス', 'required|valid_email');
            $this->form_validation->set_rules('contact_subject', '件名', 'required');
            $this->form_validation->set_rules('contact_message', '本文', 'required');

            $form_validation = $this->form_validation->run();
            if ($form_validation == false) {
                return false;
            } else {
                //send mail
                $this->sendMail($email, ADMIN_EMAIL, '', '', $subject, $body, '');
                //send success
                redirect("/contact_comp");
            }
        }
    }
}
