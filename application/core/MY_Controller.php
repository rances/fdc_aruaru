<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public $viewData = array();
	public $user;
  public $is_visitor; 
  public $meta_array = array(); // array of list meta tags
    
	function __construct() {
  
  		parent::__construct();

  		$this->load->library('twig');
  		$this->load->library('pagination');
      $this->load->library('breadCrumbs');
      $this->load->library('email');
      $this->load->model('bulletin_model');
  		$this->twig->template_dir_set(($this->agent->is_mobile()) ? TEMPLATEPATH_SP : TEMPLATEPATH_PC);

  		$this->user = UserControl::getuser();  

      $is_visitor = empty($this->user)? true : false;
  		$this->viewData['is_visitor'] = $is_visitor;
  		if($this->user){
  			$this->viewData['user'] = array(
  				'id' => $this->user['id'], 
  				'nick_name' => $this->user['nick_name']
  			);

        $this->user['user_can_point'] = (!empty($this->user['user_from_site']) && in_array($this->user['user_from_site'], array(1,2))) ? true : false;
        $this->viewData['user_can_point'] = $this->user['user_can_point'];
  		}

      

  		$this->init_meta();   
      $this->viewData['main_url'] = MAIN_URL;
      $this->viewData['twitter_account'] = TWITTER_ACOUNT;
      $this->breadcrumbs->bread_name = 'breadcrumb_list';
      $this->viewData['home_url'] = HOST_URL;
    }

    /**
     * Initialize data for meta values
     */
    public function init_meta() {
      $temp = '';
      $this->meta_array = array();
      $row = $this->bulletin_model->getCategoryMeta();
      foreach ($row as $key => $value) {
        if ($temp != $value['id']) {
            $temp = $value['id'];
            $this->meta_array[1][$temp] = array(
                  'title' => $value['b_title'],
                  'description' => $value['b_desc']
                );
            $this->meta_array[2][$value['sub_id']] = array(
                  'title' => $value['sub_title'],
                  'description' => $value['sub_description']
                );
        } else {
            $this->meta_array[2][$value['sub_id']] = array(
                  'title' => $value['sub_title'],
                  'description' => $value['sub_description']
                );
        }
      }
    }
	
    /**
     * Select meta value according to its id
     * @param string $id : ID either Main Category or Sub Category
     * @param string $type : Type if 1 = Main Category , 2 = Sub Category
     */
    public function get_meta_tags($id = null, $type = null) {
      $metag_list = $this->meta_array[$type][$id];
      $this->viewData['title'] = $metag_list['title'];
      $this->viewData['description'] = $metag_list['description'];
    }


    /**
     * name : sendMail
     * @param string $from
     * @param string $to
     * @param string $cc
     * @param string $bcc
     * @param string $subject
     * @param string $body
     * @param string $senderName
     */
    public function sendMail($from = null, $to = null, $cc = null, $bcc = null, $subject = null, $body = null, $senderName = null) {
        $from = ($from == null || $from == '') ? ADMIN_URL : $from;
        try {
              $result =$this->email
                        ->from($from,"joyspe")
                        ->to($to)
                        ->cc($cc)
                        ->bcc($bcc)
                        ->subject($subject)
                        ->message($body)
                        ->send();
        } catch (phpmailerException $e) {
            throw $e;
        } catch (Exception $e){
            throw $e;
        }
    }

	function requireLogin(){
		if(empty($this->user['id'])) 
			redirect(MAIN_URL.'user/login/');
	}


	function paginate($post, $count = 0)
    {
        if($count == 0 ) return;
        $paginate = (empty($post['pagination'])?'':$post['pagination']);
        $maxpage = 3; 
        $limit = 10; //items per page
        $page = (empty($post['page']))? 1 : $post['page'];
        $start = ($page - 1) * $limit;  

        $prev = $page - 1;          
        $next = $page + 1;       
        $lastpage = ceil($count/$limit);  
        $lpm1 = $lastpage - 1;   

        $pagination = array(
            'total_items' => $count,
            'page' => $page,
            'start' => $start,
            'prev' => $prev,
            'next' => $next,
            'lastpage' => $lastpage,
            'maxpage' => $maxpage,
            'paginate' => $paginate
        );

        return $pagination;  
    }

}


