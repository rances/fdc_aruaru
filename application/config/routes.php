<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';

$route['bulletin/ajaxPosts'] = 'bulletin/ajaxPosts';
$route['bulletin/(:any)'] = 'bulletin/index/$1';

$route['category/ajaxPosts'] = 'category/ajaxPosts';
$route['category/menu_list'] = 'category/menuList';
$route['category/(:any)'] = 'category/index/$1';

$route['statics/view/(:any)'] = 'statics/view/$1';

$route['page_theme'] = 'post/add';
$route['post/ajaxLatest'] = 'post/ajaxLatest';
$route['reaction/ajaxCommentConfirm'] = 'post/ajaxCommentConfirm';
$route['reaction/ajaxReact'] = 'post/ajaxReact';
$route['reaction/ajaxAddComment'] = 'post/ajaxAddComment';
$route['reaction/(:any)'] = 'post/index/$1';

$route['keyword'] = 'keyword/index';

/* 検証用 */
$route['t_page_theme'] = 'posttest/add';
//$route['post/ajaxLatest'] = 'posttest/ajaxLatest';
$route['t_reaction/ajaxCommentConfirm'] = 'posttest/ajaxCommentConfirm';
$route['t_reaction/ajaxReact'] = 'posttest/ajaxReact';
$route['t_reaction/ajaxAddComment'] = 'posttest/ajaxAddComment';
$route['t_reaction/(:any)'] = 'posttest/index/$1';

$route['twitter_post'] = 'twitter_post/index';
$route['twitter_api'] = 'twitter_api/index';
$route['twitter_img_api'] = 'twitter_img_api/index';
$route['twitter_text_api'] = 'twitter_text_api/index';

$route['tweet0'] = 'test/tweet0';
$route['test'] = 'test/index';
/* ----- */

$route['my_page'] = 'User/my_account';

$route['tos'] = 'Statics/view/tos';
$route['company'] = 'Statics/view/company';
$route['privacy'] = 'Statics/view/privacy';
$route['faq'] = 'Statics/view/faq';
$route['contact'] = 'Statics/view/contact';
$route['guideline'] = 'Statics/view/guideline';
$route['contact_comp'] = 'Statics/view/contact_comp';

$route['404_override'] = 'User/my404';
$route['translate_uri_dashes'] = FALSE;


//$route['uploads/temp/(:any)'] = 'uploads/temp/$1';

