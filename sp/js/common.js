// JavaScript Document

$(function() {
	//hide validate element
	$('.validate .error').hide();

	//validate
	$(".validate").validate({
		rules: {
			contact_email: {
				required: true,
				email: true
			},
			contact_subject :{
				required: true
			},
			contact_message :{
				required: true
			},
			question_category_list: {
				required: true
			},
			question_category:{
				required: true
			},
			question_subject:{
				required: true,
				maxlength:50,
				minlength:10
			},
			question_message:{
				required: true,
				maxlength:500,
				minlength:30
			},
			answer_message:{
				required: true,
				maxlength:300,
				minlength:30
			},
			consult_message:{
				required: true
			},
			question_ttl_list:{
				required: true
			}
		},
		messages: {
			contact_email: {
				required: "・メールアドレスを入力してください。",
				email: "・正しい形式で入力してください。"
			},
			contact_subject :{
				required: "・件名を入力してください。"
			},
			contact_message:{
				required: "・本文を入力してください。"
			},
			question_category_list:{
				required: "選択してください。"
			},
			question_category:{
				required: "どれか1つ選択してください。"
			},
			question_subject:{
				required: "タイトルを入力してください。",
				maxlength: "全角50文字以内で入力してください。",
				minlength: "全角10文字以上で入力してください。"
			},
			question_message:{
				required: "相談内容を入力してください。",
				maxlength: "全角500文字以内で入力してください。",
				minlength: "全角30文字以上で入力してください。",
			},
			answer_message:{
				required: "相談内容を入力してください。",
				maxlength: "全角300文字以内で入力してください。",
				minlength: "全角30文字以上で入力してください。",
			},
			consult_message:{
				required: "返信コメントを入力してください。"
			},
			question_ttl_list:{
				required: "どれか1つ選択してください。"
			}
		},
		errorElement: "p",
		errorPlacement:function(error,element){
			error.insertBefore($(element));
		}
	});
	
	//タブ切り替え
	$(".tab_content").addClass("hide");
	$(".tab_content:first").removeClass("hide");
	$("#tab_menu li").on("click",function(){
		if($(this).is(".tab_new_comment")){
			$("#tab_menu li").removeClass("current");
			$(this).addClass("current");
			$(".tab_content").addClass("hide");
			$(".content_new_comment").removeClass("hide");
		} else if($(this).is(".tab_new_arrivals")){
			$("#tab_menu li").removeClass("current");
			$(this).addClass("current");
			$(".tab_content").addClass("hide");
			$(".content_new_arrivals").removeClass("hide");
		} else if($(this).is(".tab_new_attention")){
			$("#tab_menu li").removeClass("current");
			$(this).addClass("current");
			$(".tab_content").addClass("hide");
			$(".tab_new_attention").removeClass("hide");
		} else if($(this).is(".tab_unresolved")){
			$("#tab_menu li").removeClass("current");
			$(this).addClass("current");
			$(".tab_content").addClass("hide");
			$(".content_unresolved").removeClass("hide");
		} else if($(this).is(".tab_history")){
			$("#tab_menu li").removeClass("current");
			$(this).addClass("current");
			$(".tab_content").addClass("hide");
			$(".content_history").removeClass("hide");
		} else if($(this).is(".tab_bookmark")){
			$("#tab_menu li").removeClass("current");
			$(this).addClass("current");
			$(".tab_content").addClass("hide");
			$(".content_bookmark").removeClass("hide");
		}
	});


	//modal
	$( 'a[rel*=leanModal]').leanModal({
		top: 50,
		overlay : 0.5,
		closeButton: ".modal_close"
	});

	//ui-count_form 
	$(document).on("click keydown keyup keypress change", ".ui-count_form .ui-count_input", function(){
		var scope = $(this).closest(".ui-count_form");
		var textLength = parseInt($(this).val().length);
		var maxCountLength = parseInt($(".ui-count_input",scope).attr("maxlength"));
		$(".input_count",scope).html(maxCountLength - textLength);
	});


	//評価ボタン
	$(".btn_wrap .btn_evaluate").on("click",function(){
		var scope = $(this).closest(".sec_consult_answer");
		var textNumber =$(this).val();
		$(this).removeClass("btn_red-o ic_heart-o").addClass("btn_red ic_heart");
		$(".btn_evaluate",scope).addClass("disable");
		$(this).text(textNumber + "1");
	});

	//サイドメニュー
	$("#simple-menu").sidr({
		name: 'sidr',
		side: 'right'
	});

	$('#sidr').show();
	$('#sidr').css("right", "-81.25vw");

	//返信コメント入力欄
	$("#open_comment_reply").on("click",function(){
		$("#comment_reply_form").slideDown();
		$(this).remove();
	});
});

//広告用js
jQuery(function() {
    var topBtn = jQuery('.add');
    topBtn.hide();
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 200) { // 200pxで表示
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
});

$(function() {
    $(window).scroll(function(ev) {
        var $window = $(ev.currentTarget),
            height = $window.height(),
            scrollTop = $window.scrollTop(),
            documentHeight = $(document).height();
        if (documentHeight === height + scrollTop) {
            $('.add').css('display','none');
        }
    });
});