$(function() {
	$(' .tab_new_comment, .tab_new_arrivals, .tab_new_attention').click(function(e) {
		var tab = $(this).index();
		if (tab == 0) {
			$('.page_default').show();
			$('.page_default_second').hide();
			$('.content_1').show();
			$('.content_2').hide();
			$('.content_3').hide();
		} else if (tab == 1) {
			$('.page_default').hide();
			$('.page_default_second').show();
			$('.content_1').hide();
			$('.content_2').show();
			$('.content_3').hide();
		} else if (tab == 2) {
			$('.page_default').hide();
			$('.page_default_second').show();
			$('.content_1').hide();
			$('.content_2').hide();
			$('.content_3').show();
			$('.pager_wrap').empty();
		}
	});
});