// load pagination at first based on visible list
var container = $('.tab_content:visible');
var page_container = $('.sec_bbs_list');
var content = container.data('content');
var initcount = parseInt(container.data('initcount'));

if(content.length && initcount>0){
	loadPaginator(page_container,content);
}

// reload pagination when list block is changed
$(document).on("click", ".tab_menu li", function(){	
	var container = $('.tab_content:hidden');
	page = 1;
	content = $(this).data('content');
	initcount = parseInt($(this).data('initcount'));
	if(content.length && initcount>0){
		loadPageContent(page,content,container);
		loadPaginator(page_container,content,page);
	}
});


$(document).on("click", "a.pagination", function(){	
	if($(this).hasClass('current')) return false;
	var page = $(this).data('page');
	var container = $('.tab_content:visible');
	loadPageContent(page,content,container);
	loadPaginator(page_container,content,page);
});

function loadPageContent(page,content,container){
	var container = $('.tab_content:visible');
	params = { page:page, content:content, pagination:'paginate'};
	$('#loading').show();
	doAjaxRequest(url='', params, 'HTML', function(data){
		container.html(data);
		$('html, body').animate({
       		scrollTop: $("#tab_menu").offset().top
       	}, 500);
		$('#loading').hide();
	});
}

function loadPaginator(page_container, content, page){
	if(!page_container.length || !content.length) return false;
	params = { content:content, pagination:'load', page:page};
	doAjaxRequest(url='', params, 'HTML', function(data){
		pager = $('ul.pager_wrap');
		if(pager.length){
			pager.replaceWith(data);
		}
		else{
			page_container.append(data);	
		}
		

	});
}
