var isSumitted = false;
// toggle comment form
$(document).on("click", ".open_comment_reply", function(){
	$(this).next('form').slideDown();
	$(this).siblings(".reply").fadeIn();
	$(this).remove();
	return false;
});


$(document).on("submit", ".comment_confirm_form", function(){
	var form = $(this);
	var eror_p = form.find('p.error').eq(0);
	var cid = form.data("cid");
	pid = form.data("pid");
	comment = form.find('textarea').eq(0).val();
	userfile = $("input[name='userfile']").val();
	image_file_name = $("input[name='image_file_name']").val();

	// 切り出し元の文字列
	var targetText = "a8.net,moba8.net,asp.premium-co.net,smart-c.jp,accesstrade.net,affil.jp,linkage-m.net,gamefeat.net,af-next.com,tap-in.jp";
	// "," で切り出しを行う（"ABC"、"DE"、"FGHI"の文字列配列になる）
	var ngArray = targetText.split(",");

	var search_flag = false;

	for(var i=0; i < ngArray.length; i++) {
		console.log(ngArray[i]);
		if (comment.indexOf(ngArray[i]) != -1) {
  			search_flag = true;
			break;
		}
	}
	if (search_flag == true) {
		alert('アフィリエイト目的でのご利用は禁止されています。');
		return false;
	}

	params = { pid: pid, comment: comment, cid: cid,userfile: userfile,image_file_name: image_file_name};
	eror_p.text('');
	url = "/reaction/ajaxCommentConfirm";
	$('#loading').show();
	$.ajax({
		type: "POST",
		url: url,
		dataType:"json",
		data: params
	}).done(function(data){
		$("input[name='userfile']").val('');
		$('#reaction_input_form').hide();
		$('#reaction_confirm').show();

		if(data.upload_image_path) {
			$("#ap_image").show();
			$("#ap_image #up_image").attr("src",data.upload_image_path);
		} else {
			$("#ap_image").hide();
		}

		$("#ap_message").html(data.preview);
		$('input[name="upload_image_file"]').val(data.upload_image_file);
		$('input[name="pid"]').val(data.page_id);
		$('input[name="message_all"]').val(data.message_all);
		$('input[name="bl_max"]').val(img_select_array.length);
//	    alert('success!!');
		$('#loading').hide();
	}).fail(function(data){
	    alert('エラーが発生しました。');
	});

	return false;
});

$(document).on('change','input[name="userfile"]',function(){
	var fd = new FormData();
	if ($("input[name='userfile']").val()!== '') {
		fd.append("userfile", $("input[name='userfile']").prop("files")[0] );
	}

	url = "/post/ajaxImagefileUpload";
	$.ajax({
		type: "POST",
		url: url,
		processData: false,
		contentType: false,
		data: fd
	}).done(function(data){
		$('input[name="image_file_name"]').val(data.filename);
	}).fail(function(data){
	    alert('エラーが発生しました。');
	});
});

$(document).on("click","#form_back", function(){
	$('#reaction_confirm').hide();
	$('#reaction_input_form').show();
});

/*
$(document).on("click","#btn", function(){

	for (var i = 0; i < img_select_array.length; i++) {
		if($('#bl'+i+'_title').css("display") == 'none') {
			$('#bl'+i+'_title').remove();
			$('#bl'+i+'_description').remove();
		}
	}
	$('input[name="bl_max"]').val(img_select_array.length);

	$(".image_change_btn").remove();
    $('#send_go_form').submit();
});*/

$(document).on("submit", ".comment_form_send", function(){
	if(isSumitted == false) {
		isSumitted = true;
		var form = $(this);
		var eror_p = form.find('p.error').eq(0);
		var cid = form.data("cid");
		pid = form.data("pid");

		comment = $('input[name="message_all"]').val();

		var message_all = $('input[name="message_all"]').val();
		var bl_max = $('input[name="bl_max"]').val();

		for (var i = 0; i < img_select_array.length; i++) {
			if($('#bl'+i+'_title').css("display") == 'none') {
				$('#bl'+i+'_title').remove();
				$('#bl'+i+'_description').remove();
			}
		}
//var formData = $(this);
// GETパラメータのようにシリアル化される
// 例 : name1=val1&name2=val2&name3=val3...
//var query = formData.serialize();
// 連想配列にシリアライズ
// 例 : [{name:"name1", value:"val1"}, {name:"name2", value:"val2"},...]
//var arrQuery = formData.serializeArray();
		var bl= new Array();
		var bl_title= new Array();
		var bl_description= new Array();
		var bl_source_url= new Array();
		for (var i = 0; i < img_select_array.length; i++) {
		    var v = $("input[name='bl_title["+i+"]']").val();
	        bl_title.push(v);
		    var v = $("input[name='bl_description["+i+"]']").val();
	        bl_description.push(v);
		    var v = $("input[name='bl["+i+"]']").val();
	        bl.push(v);
		    var v = $("input[name='bl_source_url["+i+"]']").val();
	        bl_source_url.push(v);
		}
		var userfile = $("input[name='upload_image_file']").val();
		params = { pid: pid, comment: comment, cid: cid, message_all:message_all, bl_max:bl_max, bl_title:bl_title,bl_description: bl_description, bl:bl, bl_source_url:bl_source_url, userfile:userfile};
		eror_p.text('');
		url = "/reaction/ajaxAddComment";

		$('#loading').show();
		doAjaxRequest(url, params, "HTML", function(data){
			if($(data).find('.new_comment')){
				check = data.split(':');
				if(check[0] == 'Error'){
					eror_p.text(check[1]).show();
				}else if (typeof cid === "undefined") {
					//PC side elements
					guideline = $(".sec_consult_answer_list");
					if(guideline.length){
						post_cont = $(".sec_consult_answer_list");
						var comment_count_all = Number($('.comment_count').text());
						var current_count = post_cont.children('.comment_area').length;
						if (comment_count_all == current_count) {
							guideline.append($(data).hide().fadeIn());
						}
						$('.comment_count').hide().text(parseInt($('.comment_count').text()) + 1).fadeIn();
						highslide_set();
						$('#reaction_confirm').hide();
						$('#reaction_input_form').show();
						$('#reaction_input_form textarea').val('');
						$('input[name="bl_max"]').val(0);
						$('input[name="message_all"]').val('');
						$("input[name='upload_image_file']").val('');
						$("up_image").attr("src","");
						img_select_array = [];
					}
					//SP side elements
					else{
						post_cont = $(".sec_consult_answer_list");
						var comment_count_all = Number($('.comment_count').text());
						var current_count = post_cont.children('.comment_area').length;
						if (comment_count_all == current_count) {
							$('.sec_response_comment').append($(data).hide().fadeIn());
						}
//						$('.sec_response_comment').append($(data).hide().fadeIn());
						$('.bbs_stutas li:first-child').html('最新コメント：<span>ちょうど今</span>');

						$('#reaction_confirm').hide();
						$('#reaction_input_form').show();
						$('#reaction_input_form textarea').val('');
						$('input[name="bl_max"]').val(0);
						$('input[name="message_all"]').val('');
						$("input[name='upload_image_file']").val('');
						$("up_image").attr("src","");
						img_select_array = [];
					}
					form.find('textarea').eq(0).val('');
					$('#loading').hide();
				}else{
					//SP side elements
					form_cont = form.parent('.comment_reply_input');
					if(form_cont.length){
						form_cont.after($(data).hide().fadeIn('slow'));
						form_cont.fadeOut('slow').remove();
					}
					//PC side elements
					else{
						form.siblings(".guideline").after($(data).hide().fadeIn('slow'));
						form.siblings(".guideline").fadeOut('slow').remove();
						form.slideToggle().remove();
						$.get("reaction_reply_template.html", function(data){
							form.append(data);
						});
					}
				}
				isSumitted = false;
			}
		});

	}

	return false;
});


// comment and reply comment
$(document).on("submit", ".comment_form", function(){
	if(isSumitted == false) {
		isSumitted = true;
		var form = $(this);
		var eror_p = form.find('p.error').eq(0);
		var cid = form.data("cid");
		pid = form.data("pid");
		comment = form.find('textarea').eq(0).val();
		params = { pid: pid, comment: comment, cid: cid };
		eror_p.text('');
		url = "/reaction/ajaxAddComment";
		doAjaxRequest(url, params, "HTML", function(data){
			if($(data).find('.new_comment')){
				check = data.split(':');
				if(check[0] == 'Error'){
					eror_p.text(check[1]).show();
				}else if (typeof cid === "undefined") {
					//PC side elements
					guideline = form.siblings(".guideline");
					if(guideline.length){
						guideline.after($(data).hide().fadeIn());
						$('.comment_count').hide().text(parseInt($('.comment_count').text()) + 1).fadeIn();
					}
					//SP side elements
					else{
						$('.sec_response_comment').prepend($(data).hide().fadeIn());
						$('.bbs_stutas li:first-child').html('最新コメント：<span>ちょうど今</span>');
					}
					form.find('textarea').eq(0).val('');
				}else{
					//SP side elements
					form_cont = form.parent('.comment_reply_input');
					if(form_cont.length){
						form_cont.before($(data).hide().fadeIn('slow'));
						var html = '<a id="open_comment_reply" class="t_link open_comment_reply" href="">このコメントに返信する▼</a>';
						form_cont.prepend(html);
						form_cont.find("textarea").val('');
						form_cont.children('form').hide();
						form_cont.children('form').slideUp();
						//form_cont.after($(data).hide().fadeIn('slow'));
						//form_cont.fadeOut('slow').remove();
					}
					//PC side elements
					else{
						form.before($(data).hide().fadeIn('slow'));
						var html ='<p class="reply open_comment_reply"><a href="#">このコメントに返信する▼</a></p>';
						form.before(html);
						form.find("textarea").val('');
						form.hide();
						form.slideUp();
						form.siblings(".guideline").fadeOut('slow');
					}
				}
				isSumitted = false;
			}
		});
	}

	return false;
});

// show more comments
$(document).on("click", ".more_comment", function(){
//	post_cont = $(".sec_consult_answer");
	post_cont = $(".sec_consult_answer_list");
	more_button = $(this);
	current_count = post_cont.children('.comment_area').length;
	elem_countr = 'comment_area">';
	post_id = $(this).data('pid');
	url = "/reaction/" + post_id;
	params = { id: post_id, offset: current_count, limit: 10, is_ajax : 1 };
	return showMore(url, params, post_cont, more_button, elem_countr);
//	return showMorePre(url, params, post_cont, more_button, elem_countr);
});

// reaction buttons
$(document).on("click", ".react_btn", function(){

	var btn = $(this);
	var post_id = btn.data('pid');
	var cid = btn.data('cid');

	// check cookie if reaction has done made already
	var cookie = $.cookie('arubbs_react');
	var reac10s = cookie ? JSON.parse(cookie) : new Array();
	reacheck = (typeof cid !== "undefined") ? 'c'+cid : 'p'+post_id;
	if(reac10s.indexOf(reacheck) > -1) return false;

	var count_contnr = (btn.next('p.count').length) ? btn.next('p.count') : btn.prev('p.count');
	var current_count = parseInt(count_contnr.text(), 10);
	
	var react = btn.data('react');
	params = { pid: post_id, react: react, cid: cid };
	url = "/reaction/ajaxReact";
	if (isSumitted == false) {
		isSumitted = true;
		doAjaxRequest(url, params, "JSON", function(data){
			if(data['success']){
				count_contnr.hide().text(current_count+1).fadeIn();
				
				if(react =='yes')
					$('.comment_like_count').hide().text(parseInt($('.comment_like_count').text()) + 1).fadeIn();

				//add reaction to cookie to avoid multiple submission
				cval_pref = (typeof cid !== "undefined") ? 'c'+cid : 'p'+post_id;
				reac10s.push(cval_pref);
				var currentDate = new Date();
				expirationDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 23, 59, 59); 
				$.cookie('arubbs_react', JSON.stringify(reac10s), {expires: expirationDate});

				// changing text size and color based on option number
				var element = '#reaction_comment_'+cid+' .consult_comment p, #reaction_comment_'+cid+' .comment_p';
				if(data['option_number'] == 1 ) {
					$(element).css({"color": 'rgb('+hexToRgb("#F8384A")+')', "font-size": "24px",  "font-weight": "bold"});
				} else if(data['option_number'] == 2 ) {
					$(element).css({"color": 'rgb('+hexToRgb("#F8384A")+')', "font-size": "18px", "font-weight": "bold"});
				} else if(data['option_number'] == 3 ) {
					$(element).css({"color": 'rgb('+hexToRgb("#2a2c37")+')', "font-size": "24px", "font-weight": "bold"});
				} else if(data['option_number'] == 4 ) {
					$(element).css({"color": 'rgb('+hexToRgb("#2a2c37")+')', "font-size": "16px", "font-weight": "normal"});
				}

				isSumitted = false;
			}
		});
	}
	return false;
});

function hexToRgb(h)
{
    var r = parseInt((cutHex(h)).substring(0,2),16), g = ((cutHex(h)).substring(2,4),16), b = parseInt((cutHex(h)).substring(4,6),16)
    return r+','+b+','+b;
}
function cutHex(h) {
	return (h.charAt(0)=="#") ? h.substring(1,7):h
}

function highslide_set(){
	var $hs=$('.c_img > a');

	var hs_length=$hs.length;

	for (var i = 0 ; i < hs_length ; i++){
	  var this_obj=$hs.eq(i);
	  this_obj.addClass('highslide');
	  this_obj.click(function(){return hs.expand(this)});
	}
}

//scroll to specific div if set at url
$(function(){
	// get hash value
	var hash = window.location.hash;
	if (hash != '') {
		if ($(hash).length) {
			// now scroll to element with that id
			$('html, body').animate({ scrollTop: $(hash).offset().top });
		}
	}
	post_cont = $(".sec_consult_answer_list");
	var comment_count_all = Number($('.comment_count').text());
	var current_count = post_cont.children('.comment_area').length;

	if ((comment_count_all > 10) && (current_count < 11)) {
		$('.more_comment').show();
	} else {
		$('.more_comment').hide();
	}
});