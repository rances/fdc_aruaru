$(function(){

$(document).on('change','input[name="userfile"]',function(){
	var fd = new FormData();
	if ($("input[name='userfile']").val()!== '') {
		fd.append("userfile", $("input[name='userfile']").prop("files")[0] );
	}
	url = "/post/ajaxImagefileUpload";
	$.ajax({
		type: "POST",
		url: url,
		processData: false,
		contentType: false,
		data: fd
	}).done(function(data){
		$('input[name="image_file_name"]').val(data.filename);
	}).fail(function(data){
	    alert('エラーが発生しました。');
	});
});

});