function doAjaxRequest(url, data, dataType, handleData )
{
	$.ajax({
		type: "POST",
		url: url,
		data: data,
		dataType: dataType,
		cache: false,
		success:
			function(data){
				handleData(data);
			}
	});
}

function doLogout(main_url) {
   var url = '/user/aruaru_logout';
    doAjaxRequest(url, '', "json", function(data){
           window.location = '/';
    });    
}

function deleteCheckedPosts(url, data) {
    var result = confirm("これを削除してもよろしいですか？");
    if (result) {
        doAjaxRequest(url, data, 'JSON', function(data){
            location.reload();
        });
        return false;
    }
}

function showMore(url, params, post_contnr, more_button, elem_countr)
{
	if(post_contnr.hasClass("all_data_shown")){
		more_button.hide();
		return false;
	} 

	if(post_contnr.hasClass("ack_data")) return false; 
	post_contnr.addClass("ack_data");

	$('#loading').show();
	doAjaxRequest(url, params, 'HTML', function(data){
		post_contnr.append($(data).hide().fadeIn());
		returned = data.split(elem_countr).length - 1;
		if(returned < 10){
			more_button.hide();
			post_contnr.addClass("all_data_shown");

		}
		post_contnr.removeClass("ack_data");
		$('#loading').hide();
	});
	return false;
}


function getCategory(bid_val, cat_val){
	var opt = '<option value="">選択してください</option>';
	$("#bbs_categorys").html(opt);
	var url = "/category/menu_list";
	var params = { bid: bid_val};
	doAjaxRequest(url, params, 'JSON', function(data){
		$.each(data, function(i, category){
			selctd = (typeof cat_val !== "undefined" && cat_val == category['id'])? 'selected=selected':'';
			opt += '<option value="'+category['id']+'"'+selctd+'>'+category['jp_name']+'</option>';
		});
		$("#bbs_categorys").html(opt);
	});
}


$(function(){

	//page theme: load category based on selected big category
	$("#bbs_big_categorys").on('change', function(){
		 getCategory($(this).val());
	});

	$("#theme_form_back_to_prev").click(function(){
		$("input[name='save']").val("0");
		$(this).closest("form").submit();
	})

});
