// JavaScript Document

$(function() {
	//hide validate element
	$('.validate .error').hide();

	//validate
	$(".validate").validate({
		rules: {
			contact_email: {
				required: true,
				email: true
			},
			contact_subject :{
				required: true
			},
			contact_message :{
				required: true
			},
			question_category_list: {
				required: true
			},
			question_category:{
				required: true
			},
			question_subject:{
				required: true,
				maxlength:30,
				minlength:10
			},
			question_message:{
				required: true,
				maxlength:500,
				minlength:30
			},
			answer_message:{
				required: true,
				maxlength:300,
				minlength:30
			},
			consult_message:{
				required: true
			},
			consult_comment:{
				required: true
			}
		},
		messages: {
			contact_email: {
				required: "メールアドレスを入力してください。",
				email: "正しい形式で入力してください。"
			},
			contact_subject :{
				required: "件名を入力してください。"
			},
			contact_message:{
				required: "本文を入力してください。"
			},
			question_category_list:{
				required: "選択してください。"
			},
			question_category:{
				required: "どれか1つ選択してください。"
			},
			question_subject:{
				required: "お題を入力してください。",
				maxlength: "全角30文字以内で入力してください。",
				minlength: "全角10文字以上で入力してください。"
			},
			question_message:{
				required: "本文を入力してください。",
				maxlength: "全角500文字以内で入力してください。",
				minlength: "全角30文字以上で入力してください。",
			},
			answer_message:{
				required: "本文を入力してください。",
				maxlength: "全角300文字以内で入力してください。",
				minlength: "全角30文字以上で入力してください。",
			},
			consult_message:{
				required: "コメントを入力してください。"
			},
			consult_comment:{
				required: "返信内容を入力してください。"
			}
		},
		errorElement: "p",
		errorPlacement:function(error,element){
			error.insertBefore($(element));
		}
	});

	//評価ボタン
	$(".btn_wrap .btn_evaluate").on("click",function(){
		var scope = $(this).closest(".sec_consult_answer");
		var textNumber =$(this).val();
		console.log(textNumber + "1");
		$(this).removeClass("btn_red-o ic_heart-o").addClass("btn_red ic_heart");
		$(".btn_evaluate",scope).addClass("disable");
		$(this).text(textNumber + "1");
	});

	//チェックボックスクリック範囲
	$(".btn_checkbox").on("click",function(){
		$(this).children("input[type=checkbox]").click();
	});
	
	//タブ切り替え
	 $(".tab_content").addClass("hide");
	$(".tab_content:first").removeClass("hide");
	$("#tab_menu li").on("click",function(){
		if($(this).is(".tab_unresolved")){
			$("#tab_menu li").removeClass("current");
			$(this).addClass("current");
			$(".tab_content").addClass("hide");
			$(".content_unresolved").removeClass("hide");
		} else if($(this).is(".tab_history")){
			$("#tab_menu li").removeClass("current");
			$(this).addClass("current");
			$(".tab_content").addClass("hide");
			$(".content_history").removeClass("hide");
		} else if($(this).is(".tab_bookmark")){
			$("#tab_menu li").removeClass("current");
			$(this).addClass("current");
			$(".tab_content").addClass("hide");
			$(".content_bookmark").removeClass("hide");
		}
	}); 


	//ui-count_form 
	$(document).on("click keydown keyup keypress change", ".ui-count_form .ui-count_input", function(){
		var scope = $(this).closest(".ui-count_form");
		var textLength = parseInt($(this).val().length);
		var maxCountLength = parseInt($(".ui-count_input",scope).attr("maxlength"));
		$(".input_count",scope).html(maxCountLength - textLength);
	});

	//modal
	$(function() {
		$( 'a[rel*=leanModal]').leanModal({
			top: 50,
			overlay : 0.5,
			closeButton: ".modal_close" 
		});
	}); 
	

	
	//scrollTop
    var topBtn = $('#page_top');    
    topBtn.hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
}); 