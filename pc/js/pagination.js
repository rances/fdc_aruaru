// load pagination at first based on visible list
var container = $('.consult_list:visible');
var content = container.data('content');
var initcount = parseInt(container.data('initcount'));
var page_container = $('.content_bottom.'+content);
if(content.length && initcount>0){
	loadPaginator(page_container,content);
}

// reload pagination when list block is changed
$(document).on("click", "#tab_menu li", function(){	
	var container = $('.consult_list:hidden');
	page = 1;
	content = $(this).data('content');
	initcount = parseInt($(this).data('initcount'));
	page_container = $('.content_bottom.'+content);
	//if(content.length && initcount>0){
		loadPageContent(page,content,container);
		loadPaginator(page_container,content,page);
	//}
});


$(document).on("click", "a.pagination", function(){	
	if ($(this).hasClass('current')) return false;
	var page = $(this).data('page');
	var container = $('.consult_list:visible');
	loadPageContent(page,content,container);
	loadPaginator(page_container,content,page);
});

function loadPageContent(page,content,container){
	var container = $('.consult_list:visible');
	params = { page:page, content:content, pagination:'paginate'};
	doAjaxRequest(url='', params, 'HTML', function(data){
		if(data.length){
			$('.tab_content:visible .search_found').hide();
			$('.tab_content:visible .content_bottom').show();
			
		}else{
			$('.tab_content:visible .search_found').show();
			$('.tab_content:visible .content_bottom').hide();
		}
		container.html(data);	
		$('html, body').animate({ scrollTop: $("#tab_menu").offset().top }, 500);		
	});
}

function loadPaginator(page_container, content, page){
	if(!page_container.length || !content.length) return false;
	params = { content:content, pagination:'load', page:page};
	doAjaxRequest(url='', params, 'HTML', function(data){
		pager = $('ul.pager_wrap');
		if(pager.length){
			pager.replaceWith(data);
		}
		else{
			page_container.append(data);	
		}
		

	});
}
